package com.antares.domain.di

import com.antares.domain.interactor.account.*
import com.antares.domain.interactor.auth.*
import com.antares.domain.interactor.season.FindSeasonByIdShowAndSeasonNumberUseCase
import com.antares.domain.interactor.season.FindSeasonByIdShowAndSeasonNumberUseCaseImp
import com.antares.domain.interactor.tvshow.*
import com.antares.domain.repository.AccountRepository
import com.antares.domain.repository.AuthRepository
import com.antares.domain.repository.SeasonRepository
import com.antares.domain.repository.TvShowRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object InteractorModule {

    @Singleton
    @Provides
    fun provideFindTvShowByIdUseCase(
        tvShowRepository: TvShowRepository
    ): FindTvShowByIdUseCase {
        return FindTvShowByIdUseCaseImp(tvShowRepository)
    }

    @Singleton
    @Provides
    fun provideFindByPopularShowUseCase(
        tvShowRepository: TvShowRepository
    ): FindByPopularTvShowUseCase {
        return FindByPopularTvShowUseCaseImp(tvShowRepository)
    }

    @Singleton
    @Provides
    fun provideFindOnTheAirShowUseCase(
        tvShowRepository: TvShowRepository
    ): FindOnTheAirShowUseCase {
        return FindOnTheAirShowUseCaseImp(tvShowRepository)
    }

    @Singleton
    @Provides
    fun provideFindOnAiringShowUseCase(
        tvShowRepository: TvShowRepository
    ): FindOnAiringTodayShowUseCase {
        return FindOnAiringTodayShowUseCaseImp(tvShowRepository)
    }

    @Singleton
    @Provides
    fun provideFindTopShowUseCase(
        tvShowRepository: TvShowRepository
    ): FindTopTvShowUseCase {
        return FindTopTvShowUseCaseImp(tvShowRepository)
    }

    @Singleton
    @Provides
    fun provideCreateNewSessionUseCase(
        authRepository: AuthRepository
    ): CreateNewSessionUseCase {
        return CreateNewSessionUseCaseImp(authRepository)
    }

    @Singleton
    @Provides
    fun provideCreateTemporaryTokenUseCase(
        authRepository: AuthRepository
    ): CreateTemporaryTokenUseCase {
        return CreateTemporaryTokenUseCaseImp(authRepository)
    }

    @Singleton
    @Provides
    fun provideFindSeasonByIdShowAndSeasonNumberUseCase(
        seasonRepository: SeasonRepository
    ): FindSeasonByIdShowAndSeasonNumberUseCase {
        return FindSeasonByIdShowAndSeasonNumberUseCaseImp(seasonRepository)
    }

    @Singleton
    @Provides
    fun provideGetAccountUseCase(
        accountRepository: AccountRepository
    ): GetAccountUseCase {
        return GetAccountUseCaseImp(accountRepository)
    }

    @Singleton
    @Provides
    fun provideDeleteSessionUseCase(
        authRepository: AuthRepository
    ): DeleteSessionUseCase {
        return DeleteSessionUseCaseImp(authRepository)
    }

    @Singleton
    @Provides
    fun provideSaveAccountDataUseCase(
        accountRepository: AccountRepository
    ): SaveAccountDataUseCase {
        return SaveAccountDataUseCaseImp(accountRepository)
    }

    @Singleton
    @Provides
    fun provideMarkTvShowAsFavoriteUseCase(
        accountRepository: AccountRepository
    ): MarkTvShowAsFavoriteUseCase {
        return MarkTvShowAsFavoriteUseCaseImp(accountRepository)
    }

    @Singleton
    @Provides
    fun provideGetFavoriteTvShowUseCase(
        accountRepository: AccountRepository
    ): GetFavoriteTvShowUseCase {
        return GetFavoriteTvShowUseCaseImp(accountRepository)
    }
}