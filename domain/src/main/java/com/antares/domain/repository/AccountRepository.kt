package com.antares.domain.repository

import com.antares.domain.model.ResultTvShow
import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.TvShow
import com.antares.domain.model.auth.Account

interface AccountRepository {
    /**
     * fetch account from API and save on cache
     */
    suspend fun getAccount(sessionId: String): ResultWrapper<Account>

    /**
     * fetch account from API and save on cache,
     * when internet is down, returns cache data
     */
    suspend fun saveAccountData(sessionId: String): ResultWrapper<Account>

    suspend fun markTvShowAsFavorite(
        sessionId: String,
        idTvShow: Int,
        favorite: Boolean
    ): ResultWrapper<TvShow>

    suspend fun getFavoriteTvShow(sessionId: String): ResultWrapper<ResultTvShow>
}