package com.antares.domain.repository

import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.Season

interface SeasonRepository {
    suspend fun findByIdShowAndSeasonNumber(idTvShow: Int, seasonNumber: Int): ResultWrapper<Season>
}