package com.antares.domain.repository

import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.auth.DeleteSession
import com.antares.domain.model.auth.GeneratedSession
import com.antares.domain.model.auth.TmpToken

interface AuthRepository {
    suspend fun createTemporaryToken(): ResultWrapper<TmpToken>
    suspend fun createNewSession(requestToken: String): ResultWrapper<GeneratedSession>
    suspend fun deleteSession(sessionId: String): ResultWrapper<DeleteSession>
}