package com.antares.domain.repository

import com.antares.domain.model.ResultTvShow
import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.TvShow

interface TvShowRepository {
    suspend fun findTopTVShow(page: Int): ResultWrapper<ResultTvShow>
    suspend fun findByPopularTVShow(page: Int): ResultWrapper<ResultTvShow>
    suspend fun findOnTheAirShowTvShow(page: Int): ResultWrapper<ResultTvShow>
    suspend fun findOnAiringTodayTVShow(page: Int): ResultWrapper<ResultTvShow>
    suspend fun findTVShowById(id: Int): ResultWrapper<TvShow>
}