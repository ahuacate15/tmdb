package com.antares.domain.repository

import com.antares.domain.model.Episode

interface EpisodesRepository {
    suspend fun findByShowAndSeason(idShow: Int, seasonNumber: Int): List<Episode>
}