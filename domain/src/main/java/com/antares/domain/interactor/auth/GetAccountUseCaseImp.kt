package com.antares.domain.interactor.auth

import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.auth.Account
import com.antares.domain.repository.AccountRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetAccountUseCaseImp @Inject constructor(
    private val accountRepository: AccountRepository
) : GetAccountUseCase {

    override suspend fun invoke(sessionId: String): ResultWrapper<Account> =
        accountRepository.getAccount(sessionId)
}