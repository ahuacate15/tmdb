package com.antares.domain.interactor.account

import com.antares.domain.repository.AccountRepository

class GetFavoriteTvShowUseCaseImp(
    private val accountRepository: AccountRepository
) : GetFavoriteTvShowUseCase {

    override suspend fun invoke(sessionId: String) = accountRepository.getFavoriteTvShow(sessionId)

}