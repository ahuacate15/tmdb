package com.antares.domain.interactor.auth

import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.auth.TmpToken

interface CreateTemporaryTokenUseCase {
    suspend operator fun invoke(): ResultWrapper<TmpToken>
}