package com.antares.domain.interactor.tvshow

import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.TvShow
import com.antares.domain.repository.TvShowRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FindTvShowByIdUseCaseImp @Inject constructor(
    private val tvShowRepository: TvShowRepository
): FindTvShowByIdUseCase {

    override suspend fun invoke(id: Int): ResultWrapper<TvShow> = tvShowRepository.findTVShowById(id)
}