package com.antares.domain.interactor.auth

import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.auth.DeleteSession

interface DeleteSessionUseCase {
    suspend fun invoke(sessionId: String): ResultWrapper<DeleteSession>
}