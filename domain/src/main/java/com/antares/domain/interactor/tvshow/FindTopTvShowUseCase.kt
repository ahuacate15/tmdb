package com.antares.domain.interactor.tvshow

import com.antares.domain.model.ResultTvShow
import com.antares.domain.model.ResultWrapper

interface FindTopTvShowUseCase {
    suspend fun invoke(page: Int): ResultWrapper<ResultTvShow>
}