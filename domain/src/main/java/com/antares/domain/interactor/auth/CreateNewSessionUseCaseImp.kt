package com.antares.domain.interactor.auth

import com.antares.domain.repository.AuthRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CreateNewSessionUseCaseImp @Inject constructor(
    private val authRepository: AuthRepository
) : CreateNewSessionUseCase {
    override suspend fun invoke(requestToken: String) =
        authRepository.createNewSession(requestToken)
}