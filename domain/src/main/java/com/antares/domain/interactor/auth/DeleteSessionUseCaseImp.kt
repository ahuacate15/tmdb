package com.antares.domain.interactor.auth

import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.auth.DeleteSession
import com.antares.domain.repository.AuthRepository

class DeleteSessionUseCaseImp(
    private val authRepository: AuthRepository
) : DeleteSessionUseCase {

    override suspend fun invoke(sessionId: String): ResultWrapper<DeleteSession> =
        authRepository.deleteSession(sessionId)
}