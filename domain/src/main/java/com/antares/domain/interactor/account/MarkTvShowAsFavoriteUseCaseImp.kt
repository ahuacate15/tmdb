package com.antares.domain.interactor.account

import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.TvShow
import com.antares.domain.repository.AccountRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MarkTvShowAsFavoriteUseCaseImp @Inject constructor(
    private val accountRepository: AccountRepository
) : MarkTvShowAsFavoriteUseCase {

    override suspend fun invoke(
        sessionId: String,
        idTvShow: Int,
        favorite: Boolean
    ): ResultWrapper<TvShow> =
        accountRepository.markTvShowAsFavorite(sessionId, idTvShow, favorite)
}