package com.antares.domain.interactor.tvshow

import com.antares.domain.model.Episode
import com.antares.domain.repository.EpisodesRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FindEpisodesByTvShowAndSeasonUseCaseImp @Inject constructor(
    private val episodesRepository: EpisodesRepository
) : FindEpisodesByTvShowAndSeasonUseCase {

    override suspend fun invoke(id: Int, seasonNumber: Int): List<Episode> =
        episodesRepository.findByShowAndSeason(id, seasonNumber)
}