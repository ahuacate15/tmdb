package com.antares.domain.interactor.account

import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.TvShow

interface MarkTvShowAsFavoriteUseCase {
    suspend fun invoke(sessionId: String, idTvShow: Int, favorite: Boolean): ResultWrapper<TvShow>
}