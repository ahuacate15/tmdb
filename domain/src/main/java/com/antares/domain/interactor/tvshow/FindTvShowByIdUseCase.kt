package com.antares.domain.interactor.tvshow

import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.TvShow

interface FindTvShowByIdUseCase {
    suspend fun invoke(id: Int): ResultWrapper<TvShow>
}