package com.antares.domain.interactor.season

import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.Season

interface FindSeasonByIdShowAndSeasonNumberUseCase {
    suspend fun invoke(idTvShow: Int, seasonNumber: Int): ResultWrapper<Season>
}