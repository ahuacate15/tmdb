package com.antares.domain.interactor.account

import com.antares.domain.model.ResultTvShow
import com.antares.domain.model.ResultWrapper

interface GetFavoriteTvShowUseCase {
    suspend fun invoke(sessionId: String): ResultWrapper<ResultTvShow>
}