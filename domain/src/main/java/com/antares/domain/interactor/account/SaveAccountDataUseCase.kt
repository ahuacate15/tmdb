package com.antares.domain.interactor.account

import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.auth.Account

interface SaveAccountDataUseCase {
    suspend fun invoke(sessionId: String): ResultWrapper<Account>
}