package com.antares.domain.interactor.tvshow

import com.antares.domain.model.ResultTvShow
import com.antares.domain.model.ResultWrapper
import com.antares.domain.repository.TvShowRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FindTopTvShowUseCaseImp @Inject constructor(
    private val tvShowRepository: TvShowRepository
): FindTopTvShowUseCase {

    override suspend fun invoke(page: Int): ResultWrapper<ResultTvShow> = tvShowRepository.findTopTVShow(page)
}