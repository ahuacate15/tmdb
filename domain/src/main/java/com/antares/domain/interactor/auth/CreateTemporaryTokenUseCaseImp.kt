package com.antares.domain.interactor.auth

import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.auth.TmpToken
import com.antares.domain.repository.AuthRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CreateTemporaryTokenUseCaseImp @Inject constructor(
    private val authRepository: AuthRepository
): CreateTemporaryTokenUseCase {
    override suspend operator fun invoke(): ResultWrapper<TmpToken> = authRepository.createTemporaryToken()
}