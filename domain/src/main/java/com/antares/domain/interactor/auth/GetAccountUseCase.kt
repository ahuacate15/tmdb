package com.antares.domain.interactor.auth

import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.auth.Account

interface GetAccountUseCase {
    suspend fun invoke(sessionId: String): ResultWrapper<Account>
}