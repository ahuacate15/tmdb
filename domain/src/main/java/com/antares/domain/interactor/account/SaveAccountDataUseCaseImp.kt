package com.antares.domain.interactor.account

import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.auth.Account
import com.antares.domain.repository.AccountRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SaveAccountDataUseCaseImp @Inject constructor(
    val accountRepository: AccountRepository
) : SaveAccountDataUseCase {

    override suspend fun invoke(sessionId: String): ResultWrapper<Account> =
        accountRepository.saveAccountData(sessionId)
}