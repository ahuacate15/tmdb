package com.antares.domain.interactor.auth

import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.auth.GeneratedSession

interface CreateNewSessionUseCase {
    suspend fun invoke(requestToken: String): ResultWrapper<GeneratedSession>
}