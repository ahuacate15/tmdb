package com.antares.domain.interactor.season

import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.Season
import com.antares.domain.repository.SeasonRepository
import javax.inject.Inject

class FindSeasonByIdShowAndSeasonNumberUseCaseImp @Inject constructor(
    private val seasonRepository: SeasonRepository
) : FindSeasonByIdShowAndSeasonNumberUseCase {

    override suspend fun invoke(idTvShow: Int, seasonNumber: Int): ResultWrapper<Season> =
        seasonRepository.findByIdShowAndSeasonNumber(idTvShow, seasonNumber)
}