package com.antares.domain.interactor.tvshow

import com.antares.domain.model.Episode

interface FindEpisodesByTvShowAndSeasonUseCase {
    suspend fun invoke(id: Int, seasonNumber: Int) : List<Episode>
}