package com.antares.domain.model.auth

class GeneratedSession(
    val success: Boolean,
    val sessionId: String
)