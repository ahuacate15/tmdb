package com.antares.domain.model

class Episode(
    val id: Int,
    val name: String,
    val overview: String,
    val episodeNumber: Int,
    val stillPath: String?
)