package com.antares.domain.model

class GenericDomainResponse(
    val statusCode: Int
)