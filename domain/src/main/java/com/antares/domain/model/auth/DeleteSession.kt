package com.antares.domain.model.auth

class DeleteSession(
    val success: Boolean
)