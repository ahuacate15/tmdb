package com.antares.domain.model.auth

class Account(
    val id: Int,
    val name: String,
    val username: String,
    val avatar: String?
)