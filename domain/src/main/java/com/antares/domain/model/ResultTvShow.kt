package com.antares.domain.model

class ResultTvShow(
    val page: Int,
    val totalPages: Int,
    val totalResults: Int,
    val results : List<TvShow>
)