package com.antares.domain.model

class TvShow(
    val id: Int,
    val name: String,
    val originalName: String,
    val posterPath: String? = "",
    val voteAverage: Float,
    val popularity: Float,
    val overview: String,
    val favorite: Boolean = false,
    val seasons: List<Season>?
)