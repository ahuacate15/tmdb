package com.antares.domain.model

class Season(
    val id: Int,
    val name: String,
    val overview: String,
    val posterPath: String?,
    val seasonNumber: Int,
    val episodeCount: Int,
    val episodes: List<Episode>?
)