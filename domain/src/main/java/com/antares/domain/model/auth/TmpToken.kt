package com.antares.domain.model.auth

class TmpToken(
    val success: Boolean,
    val requestToken: String
)