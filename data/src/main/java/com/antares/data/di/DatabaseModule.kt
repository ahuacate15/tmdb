package com.antares.data.di

import android.content.Context
import com.antares.data.data.AppRoomContainer
import com.antares.data.data.dao.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Provides
    @Singleton
    fun provideAppRoomDB(
        @ApplicationContext context: Context
    ): AppRoomContainer {
        return AppRoomContainer(context)
    }

    @Provides
    @Singleton
    fun provideTvShowDao(
        appRoomContainer: AppRoomContainer
    ): TvShowDao {
        return appRoomContainer.appRoomDB.TvShowDao()
    }

    @Provides
    @Singleton
    fun provideSeasonDao(
        appRoomContainer: AppRoomContainer
    ): SeasonDao {
        return appRoomContainer.appRoomDB.SeasonDao()
    }

    @Provides
    @Singleton
    fun provideEpisodeDao(
        appRoomContainer: AppRoomContainer
    ): EpisodeDao {
        return appRoomContainer.appRoomDB.EpisodeDao()
    }

    @Provides
    @Singleton
    fun provideAccountDao(
        appRoomContainer: AppRoomContainer
    ): AccountDao {
        return appRoomContainer.appRoomDB.AccountDao()
    }

    @Provides
    @Singleton
    fun provideTvShowOnAirDao(
        appRoomContainer: AppRoomContainer
    ): TvShowOnAirDao {
        return appRoomContainer.appRoomDB.TvShowOnAirDao()
    }

    @Provides
    @Singleton
    fun provideTvShowAiringTodayDao(
        appRoomContainer: AppRoomContainer
    ): TvShowAiringTodayDao {
        return appRoomContainer.appRoomDB.TvShowAiringTodayDao()
    }

}