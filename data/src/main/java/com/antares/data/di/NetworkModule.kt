package com.antares.data.di

import com.antares.data.networking.RetrofitContainer
import com.antares.data.networking.api.AccountApi
import com.antares.data.networking.api.AuthApi
import com.antares.data.networking.api.SeasonApi
import com.antares.data.networking.api.TvShowApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun provideRetrofit(): RetrofitContainer = RetrofitContainer()

    @Singleton
    @Provides
    fun provideAuthApi(
        retrofitContainer: RetrofitContainer
    ): AuthApi {
        return retrofitContainer.retrofit.create(AuthApi::class.java)
    }

    @Singleton
    @Provides
    fun provideTvShowApi(
        retrofitContainer: RetrofitContainer
    ): TvShowApi {
        return retrofitContainer.retrofit.create(TvShowApi::class.java)
    }

    @Singleton
    @Provides
    fun provideSeasonApi(
        retrofitContainer: RetrofitContainer
    ): SeasonApi {
        return retrofitContainer.retrofit.create(SeasonApi::class.java)
    }

    @Singleton
    @Provides
    fun provideAccountApi(
        retrofitContainer: RetrofitContainer
    ): AccountApi {
        return retrofitContainer.retrofit.create(AccountApi::class.java)
    }
}