package com.antares.data.di

import android.content.Context
import com.antares.data.coroutine.CoroutineContextProvider
import com.antares.data.data.dao.*
import com.antares.data.networking.api.AccountApi
import com.antares.data.networking.api.AuthApi
import com.antares.data.networking.api.SeasonApi
import com.antares.data.networking.api.TvShowApi
import com.antares.data.repository.AccountRepositoryImp
import com.antares.data.repository.AuthRepositoryImp
import com.antares.data.repository.SeasonRepositoryImp
import com.antares.data.repository.TvShowRepositoryImp
import com.antares.data.utils.Connectivity
import com.antares.data.utils.ConnectivityImp
import com.antares.domain.repository.AccountRepository
import com.antares.domain.repository.AuthRepository
import com.antares.domain.repository.SeasonRepository
import com.antares.domain.repository.TvShowRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Singleton
    @Provides
    fun provideConnectivity(
        @ApplicationContext context: Context
    ): Connectivity {
        return ConnectivityImp(context)
    }

    @Singleton
    @Provides
    fun provideAuthRepository(
        authApi: AuthApi,
        connectivity: Connectivity,
        coroutineContextProvider: CoroutineContextProvider
    ): AuthRepository {
        return AuthRepositoryImp(authApi, connectivity, coroutineContextProvider)
    }

    @Singleton
    @Provides
    fun provideTvShowRepository(
        tvShowApi: TvShowApi,
        tvShowDao: TvShowDao,
        tvShowOnAirDao: TvShowOnAirDao,
        tvShowAiringTodayDao: TvShowAiringTodayDao,
        seasonDao: SeasonDao,
        accountDao: AccountDao,
        connectivity: Connectivity,
        coroutineContextProvider: CoroutineContextProvider
    ): TvShowRepository {
        return TvShowRepositoryImp(
            tvShowApi,
            tvShowDao,
            tvShowOnAirDao,
            tvShowAiringTodayDao,
            seasonDao,
            accountDao,
            connectivity,
            coroutineContextProvider
        )
    }

    @Singleton
    @Provides
    fun provideSeasonRepository(
        seasonApi: SeasonApi,
        seasonDao: SeasonDao,
        episodeDao: EpisodeDao,
        connectivity: Connectivity,
        coroutineContextProvider: CoroutineContextProvider
    ): SeasonRepository {
        return SeasonRepositoryImp(
            seasonApi,
            seasonDao,
            episodeDao,
            connectivity,
            coroutineContextProvider
        )
    }

    @Singleton
    @Provides
    fun provideAccountRepository(
        accountApi: AccountApi,
        accountDao: AccountDao,
        tvShowDao: TvShowDao,
        coroutineContextProvider: CoroutineContextProvider,
        connectivity: Connectivity
    ): AccountRepository {
        return AccountRepositoryImp(
            accountApi,
            accountDao,
            tvShowDao,
            coroutineContextProvider,
            connectivity
        )
    }
}