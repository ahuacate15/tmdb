package com.antares.data.utils

interface DomainEntity<T> {
    fun mapToDomain() : T
}