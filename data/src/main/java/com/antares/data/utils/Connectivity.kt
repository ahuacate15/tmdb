package com.antares.data.utils

interface Connectivity {
    fun hasNetworkAccess(): Boolean
}