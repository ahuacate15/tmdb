package com.antares.data.utils

import com.antares.data.networking.response.ErrorResponse
import com.antares.domain.model.ResultWrapper
import com.google.gson.Gson
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException
import kotlin.coroutines.CoroutineContext

suspend fun <T> safeApiCall(
    dispatcher: CoroutineContext,
    apiCall: suspend () -> T
): ResultWrapper<T> {
    return withContext(dispatcher) {
        try {
            ResultWrapper.Success(apiCall.invoke())
        } catch (throwable: Throwable) {
            when (throwable) {
                is IOException -> ResultWrapper.NetworkError
                is HttpException -> {
                    val errorResponse: ErrorResponse? = convertErrorBody(throwable)
                    ResultWrapper.GenericError(
                        errorResponse?.statusCode,
                        errorResponse?.statusMessage
                    )
                }
                else -> {
                    ResultWrapper.GenericError(null, null)
                }
            }
        }
    }
}

private fun convertErrorBody(throwable: HttpException): ErrorResponse? {
    return try {
        throwable.response()?.errorBody().let { safeBody ->
            val gson = Gson()
            val string = safeBody?.string()
            gson.fromJson(string, ErrorResponse::class.java)
        }
    } catch (e: Exception) {
        null
    }
}
