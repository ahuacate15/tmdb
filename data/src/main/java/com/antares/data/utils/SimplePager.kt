package com.antares.data.utils

class SimplePager(
    private val pageNumber: Int = 0,
    val pageSize: Int = 20,
    val totalRows: Int
) {

    /**
     * calculate number of pages on total data, used only on cache data
     */
    fun getTotalPages() = totalRows / pageSize + if (totalRows % pageSize > 0) 1 else 0

    /**
     * calculate offset to sql queries
     * SELECT * FROM table OFFSET x
     */
    fun getOffset(): Int {
        val offset = pageSize * (pageNumber - 1)
        return if (offset < 0) 0 else offset
    }

}