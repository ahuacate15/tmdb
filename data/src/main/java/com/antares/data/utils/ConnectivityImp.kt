package com.antares.data.utils

import android.content.Context
import android.net.ConnectivityManager

class ConnectivityImp(
    private val context: Context
): Connectivity {

    override fun hasNetworkAccess(): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val currentNetwork = connectivityManager.activeNetwork
        return currentNetwork != null
    }
}