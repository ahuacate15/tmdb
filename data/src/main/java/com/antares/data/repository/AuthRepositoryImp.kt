package com.antares.data.repository

import com.antares.data.coroutine.CoroutineContextProvider
import com.antares.data.networking.api.AuthApi
import com.antares.data.networking.request.DeleteSessionRequest
import com.antares.data.networking.request.RequestToken
import com.antares.data.utils.Connectivity
import com.antares.data.utils.safeApiCall
import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.auth.DeleteSession
import com.antares.domain.model.auth.GeneratedSession
import com.antares.domain.model.auth.TmpToken
import com.antares.domain.repository.AuthRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthRepositoryImp @Inject constructor(
    private val authApi: AuthApi,
    connectivity: Connectivity,
    private val dispatcher: CoroutineContextProvider
) : BaseRepository(connectivity, dispatcher), AuthRepository {

    override suspend fun createTemporaryToken(): ResultWrapper<TmpToken> =
        safeApiCall(dispatcher.io) { authApi.createTemporaryToken().mapToDomain() }

    override suspend fun createNewSession(requestToken: String): ResultWrapper<GeneratedSession> =
        safeApiCall(dispatcher.io) {
            authApi.createNewSession(RequestToken(requestToken)).mapToDomain()
        }

    override suspend fun deleteSession(sessionId: String): ResultWrapper<DeleteSession> =
        safeApiCall(dispatcher.io) {
            val deleteSession = authApi.deleteSession(DeleteSessionRequest(sessionId))
            deleteSession.mapToDomain()
        }
}