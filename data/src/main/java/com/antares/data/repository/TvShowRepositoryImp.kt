package com.antares.data.repository

import com.antares.data.coroutine.CoroutineContextProvider
import com.antares.data.data.dao.*
import com.antares.data.data.entity.TvShowAiringTodayEntity
import com.antares.data.data.entity.TvShowEntity
import com.antares.data.data.entity.TvShowOnTvEntity
import com.antares.data.networking.api.TvShowApi
import com.antares.data.utils.Connectivity
import com.antares.data.utils.SimplePager
import com.antares.domain.model.ResultTvShow
import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.TvShow
import com.antares.domain.repository.TvShowRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TvShowRepositoryImp @Inject constructor(
    private val tvShowApi: TvShowApi,
    private val tvShowDao: TvShowDao,
    private val tvShowOnAirDao: TvShowOnAirDao,
    private val tvShowAiringTodayDao: TvShowAiringTodayDao,
    private val seasonDao: SeasonDao,
    private val accountDao: AccountDao,
    connectivity: Connectivity,
    dispatcher: CoroutineContextProvider
) : BaseRepository(connectivity, dispatcher), TvShowRepository {

    override suspend fun findTopTVShow(page: Int): ResultWrapper<ResultTvShow> =
        fetchData(
            apiDataProvider = {
                val dataApi = tvShowApi.findTopShow(page)
                tvShowDao.insertShow(dataApi.results.map { it.mapToEntity() })
                dataApi.mapToDomain()
            },
            dbDataProvider = {
                parseCacheResult(page, tvShowDao.countTotalShow(), tvShowDao::findTopShow)
            }
        )

    override suspend fun findByPopularTVShow(page: Int): ResultWrapper<ResultTvShow> =
        fetchData(
            apiDataProvider = {
                val dataApi = tvShowApi.findByPopularShow(page)

                // update cache of popular shows
                tvShowDao.insertShow(dataApi.results.map { it.mapToEntity() })

                dataApi.mapToDomain()
            },
            dbDataProvider = {
                parseCacheResult(page, tvShowDao.countTotalShow(), tvShowDao::findByPopular)
            }
        )

    override suspend fun findOnTheAirShowTvShow(page: Int): ResultWrapper<ResultTvShow> = fetchData(
        apiDataProvider = {
            val dataApi = tvShowApi.findOnTheAirShow(page)

            /**
             * unMark all onAir tvShow
             * This data changes daily, it is necessary that it is updated with most recent onAirTvShow list
             */
            tvShowOnAirDao.unMarkOnAirShow()
            tvShowOnAirDao.insertOnAirShow(dataApi.results.map {
                TvShowOnTvEntity(
                    idTvShow = it.id,
                    onTv = true
                )
            })
            dataApi.mapToDomain()
        },
        dbDataProvider = {
            parseCacheResult(
                page = page,
                totalRows = tvShowOnAirDao.countOnAirShow(),
                fetchData = tvShowOnAirDao::findOnAirByPopular
            )
        }
    )

    override suspend fun findOnAiringTodayTVShow(page: Int): ResultWrapper<ResultTvShow> =
        fetchData(
            apiDataProvider = {
                val dataApi = tvShowApi.findOnAiringTodayShow(page)

                /**
                 * unMark all onAir tvShow
                 * This data changes daily, it is necessary that it is updated with most recent onAirTvShow list
                 */
                tvShowAiringTodayDao.unMarkAiringTodayShow()
                tvShowAiringTodayDao.insertAiringTodayShow(dataApi.results.map {
                    TvShowAiringTodayEntity(
                        idTvShow = it.id,
                        onAiringToday = true
                    )
                })
                dataApi.mapToDomain()
            },
            dbDataProvider = {
                parseCacheResult(
                    page = page,
                    totalRows = tvShowAiringTodayDao.countAiringTodayShow(),
                    fetchData = tvShowAiringTodayDao::findAiringTodayByPopular
                )
            }
        )


    override suspend fun findTVShowById(id: Int): ResultWrapper<TvShow> = fetchData(
        apiDataProvider = {
            val dataApi = tvShowApi.findById(id)

            /**
             * verify if show has been mark as favorite
             * when result is null, the tvShow is not a favorite item
             */
            val isMarkedAsFavorite = accountDao.verifyFavoriteShow(id)

            //update cache on tvShow
            tvShowDao.insertShow(listOf(dataApi.mapToEntity()))

            //save list of seasons on cache
            dataApi.seasons?.let { safeSeasonList ->
                seasonDao.insertSeason(safeSeasonList.map { it.mapToEntity(id) })
            }

            dataApi.mapToDomain(
                favorite = isMarkedAsFavorite != null
            )
        },
        dbDataProvider = {
            //fetch seasons of tvShow
            val seasons = seasonDao.findByIdShow(id)

            /**
             * verify if show has been mark as favorite
             * when result is null, the tvShow is not a favorite item
             */
            val isMarkedAsFavorite = accountDao.verifyFavoriteShow(id)

            tvShowDao.findById(id).mapToDomain(
                favorite = isMarkedAsFavorite != null,
                seasons = seasons.map { it.mapToDomain(listOf()) }
            )
        }
    )

    private suspend fun parseCacheResult(
        page: Int,
        totalRows: Int,
        fetchData: suspend (Int, Int) -> List<TvShowEntity>
    ): ResultTvShow {

        val simplePager = SimplePager(
            pageNumber = page,
            totalRows = totalRows
        )

        val data = fetchData(simplePager.pageSize, simplePager.getOffset())
        return ResultTvShow(
            page = page,
            totalPages = simplePager.getTotalPages(),
            totalResults = simplePager.totalRows,
            results = data.map { it.mapToDomain() }
        )
    }
}