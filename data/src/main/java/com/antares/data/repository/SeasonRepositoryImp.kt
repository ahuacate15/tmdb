package com.antares.data.repository

import com.antares.data.coroutine.CoroutineContextProvider
import com.antares.data.data.dao.EpisodeDao
import com.antares.data.data.dao.SeasonDao
import com.antares.data.networking.api.SeasonApi
import com.antares.data.utils.Connectivity
import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.Season
import com.antares.domain.repository.SeasonRepository

class SeasonRepositoryImp(
    private val seasonApi: SeasonApi,
    private val seasonDao: SeasonDao,
    private val episodeDao: EpisodeDao,
    connectivity: Connectivity,
    dispatcher: CoroutineContextProvider
) : BaseRepository(connectivity, dispatcher), SeasonRepository {

    override suspend fun findByIdShowAndSeasonNumber(
        idTvShow: Int,
        seasonNumber: Int
    ): ResultWrapper<Season> =
        fetchData(
            apiDataProvider = {
                val data = seasonApi.findByIdShowAndSeasonNumber(idTvShow, seasonNumber)

                //update cache of season
                seasonDao.insertSeason(listOf(data.mapToEntity(idTvShow)))

                data.episodes?.let { safeEpisodeList ->
                    //update cache of episode list
                    episodeDao.insertEpisode(safeEpisodeList.map {
                        it.mapToEntity(
                            idTvShow,
                            seasonNumber
                        )
                    })
                }
                data.mapToDomain()
            },
            dbDataProvider = {
                //find season by ID and seasonNumber
                val season = seasonDao.findByIdShowAndSeasonNumber(idTvShow, seasonNumber)
                //find list of episodes of show and season
                val episodeList = episodeDao.findByIdShowAndSeasonNumber(idTvShow, seasonNumber)
                    .map { it.mapToDomain() }
                season.mapToDomain(episodeList)
            }
        )
}