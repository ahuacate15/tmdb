package com.antares.data.repository

import com.antares.data.coroutine.CoroutineContextProvider
import com.antares.data.utils.Connectivity
import com.antares.data.utils.safeApiCall
import com.antares.domain.model.ResultWrapper

open class BaseRepository(
    private val connectivity: Connectivity,
    private val dispatcher: CoroutineContextProvider
) {

    /**
     * handle repository logic, returning network data when internet
     * connection is up and cache data when connection is down
     */
    suspend fun <T> fetchData(
        apiDataProvider: suspend () -> T,
        dbDataProvider: suspend () -> T
    ): ResultWrapper<T> = safeApiCall(dispatcher.io) {
        if (connectivity.hasNetworkAccess()) {
            apiDataProvider()
        } else {
            dbDataProvider()
        }
    }
}