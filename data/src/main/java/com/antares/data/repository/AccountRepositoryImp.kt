package com.antares.data.repository

import com.antares.data.coroutine.CoroutineContextProvider
import com.antares.data.data.dao.AccountDao
import com.antares.data.data.dao.TvShowDao
import com.antares.data.data.entity.TvShowFavoriteEntity
import com.antares.data.networking.api.AccountApi
import com.antares.data.networking.request.MarkAsFavoriteRequest
import com.antares.data.utils.Connectivity
import com.antares.data.utils.safeApiCall
import com.antares.domain.model.ResultTvShow
import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.auth.Account
import com.antares.domain.repository.AccountRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AccountRepositoryImp @Inject constructor(
    private val accountApi: AccountApi,
    private val accountDao: AccountDao,
    private val tvShowDao: TvShowDao,
    private val dispatcher: CoroutineContextProvider,
    connectivity: Connectivity
) : BaseRepository(connectivity, dispatcher), AccountRepository {

    override suspend fun getAccount(sessionId: String): ResultWrapper<Account> = fetchData(
        apiDataProvider = {
            //fetch account from network
            val account = accountApi.getAccount(sessionId)

            //save account data on cache
            accountDao.insertAccount(account.mapToEntity(sessionId))

            account.mapToDomain()
        },
        dbDataProvider = {
            //find account from cache
            val account = accountDao.findBySessionId(sessionId)
            account.mapToDomain()
        }
    )

    override suspend fun saveAccountData(sessionId: String): ResultWrapper<Account> =
        safeApiCall(dispatcher.io) {
            //fetch account from network
            val account = accountApi.getAccount(sessionId)
            //save account data on cache
            accountDao.insertAccount(account.mapToEntity(sessionId))
            account.mapToDomain()
        }

    override suspend fun markTvShowAsFavorite(sessionId: String, idTvShow: Int, favorite: Boolean) =
        safeApiCall(dispatcher.io) {
            //prepare parameter
            val favoriteTvShow = MarkAsFavoriteRequest(
                mediaId = idTvShow,
                favorite = favorite
            )
            //fetch account data from network
            val account = accountApi.getAccount(sessionId)

            //mark tvShow as favorite
            accountApi.markTvShowAsFavorite(account.id, sessionId, favoriteTvShow)

            //update tvShowFavorite cache
            accountDao.maskShowAsFavorite(
                TvShowFavoriteEntity(
                    idTvShow = idTvShow,
                    favorite = favorite
                )
            )

            //fetch cache tvShow and parse to entity, with favorite parameter
            val cacheTvShow = tvShowDao.findById(idTvShow)

            cacheTvShow.mapToDomain(
                favorite = favorite
            )
        }

    override suspend fun getFavoriteTvShow(sessionId: String): ResultWrapper<ResultTvShow> =
        fetchData(
            apiDataProvider = {
                //fetch account info
                val account = accountApi.getAccount(sessionId)

                //fetch favorite showTv using session and account info
                val result = accountApi.getFavoriteTvShow(account.id, sessionId)

                //save data on cache, mark shows as favorite
                tvShowDao.insertShow(result.results.map { it.mapToEntity() })

                result.mapToDomain()
            },
            dbDataProvider = {
                //get data from cache and mock resultTvShow on a single page
                val result = accountDao.findFavorite()
                ResultTvShow(
                    page = 1,
                    totalPages = 1,
                    totalResults = result.size,
                    results = result.map { it.mapToDomain() }
                )
            }
        )
}