package com.antares.data.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class TvShowAiringTodayEntity(
    @PrimaryKey
    val idTvShow: Int,

    @ColumnInfo(name = "on_airing_today")
    val onAiringToday: Boolean
)