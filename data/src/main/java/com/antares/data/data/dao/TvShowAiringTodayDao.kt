package com.antares.data.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.antares.data.data.entity.TvShowAiringTodayEntity
import com.antares.data.data.entity.TvShowEntity

@Dao
interface TvShowAiringTodayDao {

    @Query("SELECT count(0) FROM TvShowAiringTodayEntity WHERE on_airing_today")
    suspend fun countAiringTodayShow(): Int

    @Query("UPDATE TvShowAiringTodayEntity SET on_airing_today = 0")
    suspend fun unMarkAiringTodayShow()

    @Query("SELECT tvShow.* FROM TvShowEntity tvShow INNER JOIN TvShowAiringTodayEntity airingToday ON tvShow.id = airingToday.idTvShow WHERE airingToday.on_airing_today ORDER BY popularity DESC LIMIT :pageSize OFFSET :offset")
    suspend fun findAiringTodayByPopular(pageSize: Int, offset: Int): List<TvShowEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAiringTodayShow(entities: List<TvShowAiringTodayEntity>)

}