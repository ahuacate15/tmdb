package com.antares.data.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.antares.domain.model.Episode
import com.antares.domain.model.Season


@Entity
class SeasonEntity(
    @PrimaryKey
    val id: Int,

    @ColumnInfo(name = "tv_show_id")
    val tvShowId: Int,

    @ColumnInfo(name = "name")
    val name: String,

    @ColumnInfo(name = "poster_path")
    val posterPath: String?,

    @ColumnInfo(name = "overview")
    val overview: String,

    @ColumnInfo(name = "season_number")
    val seasonNumber: Int,

    @ColumnInfo(name = "episode_count")
    val episodeCount: Int
) {

    fun mapToDomain(listEpisode: List<Episode>?) = Season(
        id = id,
        name = name,
        posterPath = posterPath,
        overview = overview,
        episodeCount = episodeCount,
        seasonNumber = seasonNumber,
        episodes = listEpisode
    )

}