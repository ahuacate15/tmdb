package com.antares.data.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.antares.data.data.entity.EpisodeEntity

@Dao
interface EpisodeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertEpisode(entities: List<EpisodeEntity>)

    @Query("SELECT * FROM EpisodeEntity WHERE id_tv_show = :idTvShow and season_number = :seasonNumber")
    suspend fun findByIdShowAndSeasonNumber(idTvShow: Int, seasonNumber: Int): List<EpisodeEntity>
}