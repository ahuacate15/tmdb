package com.antares.data.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.antares.domain.model.Season
import com.antares.domain.model.TvShow

@Entity
class TvShowEntity(
    @PrimaryKey
    val id: Int,

    @ColumnInfo(name = "name")
    val name: String,

    @ColumnInfo(name = "original_name")
    val originalName: String,

    @ColumnInfo(name = "poster_path")
    val posterPath: String?,

    @ColumnInfo(name = "vote_average")
    val voteAverage: Float,

    @ColumnInfo(name = "popularity")
    val popularity: Float,

    @ColumnInfo(name = "overview")
    val overview: String
) {

    fun mapToDomain(favorite: Boolean = false, seasons: List<Season>? = null): TvShow =
        TvShow(
            id = id,
            name = name,
            originalName = originalName,
            posterPath = posterPath,
            voteAverage = voteAverage,
            popularity = popularity,
            overview = overview,
            favorite = favorite,
            seasons = seasons
        )
}