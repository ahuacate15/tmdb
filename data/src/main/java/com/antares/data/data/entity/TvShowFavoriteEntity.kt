package com.antares.data.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class TvShowFavoriteEntity(
    @PrimaryKey
    val idTvShow: Int,

    @ColumnInfo(name = "favorite")
    val favorite: Boolean

)