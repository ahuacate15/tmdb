package com.antares.data.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class TvShowOnTvEntity(
    @PrimaryKey
    val idTvShow: Int,

    @ColumnInfo(name = "on_tv")
    val onTv: Boolean
)