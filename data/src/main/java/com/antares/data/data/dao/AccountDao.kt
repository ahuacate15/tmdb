package com.antares.data.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.antares.data.data.entity.AccountEntity
import com.antares.data.data.entity.TvShowEntity
import com.antares.data.data.entity.TvShowFavoriteEntity

@Dao
interface AccountDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAccount(account: AccountEntity)

    @Query("SELECT * FROM AccountEntity WHERE session_id = :sessionId")
    suspend fun findBySessionId(sessionId: String): AccountEntity

    @Query("SELECT show.* FROM TvShowEntity show INNER JOIN TvShowFavoriteEntity favorite on show.id = favorite.favorite WHERE favorite.favorite")
    suspend fun findFavorite(): List<TvShowEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun maskShowAsFavorite(entity: TvShowFavoriteEntity)

    @Query("SELECT * FROM TvShowFavoriteEntity WHERE idTvShow = :idTvShow AND favorite")
    suspend fun verifyFavoriteShow(idTvShow: Int): TvShowFavoriteEntity?
}