package com.antares.data.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.antares.data.data.entity.TvShowEntity
import com.antares.data.data.entity.TvShowOnTvEntity

@Dao
interface TvShowOnAirDao {

    @Query("SELECT count(0) FROM TvShowOnTvEntity WHERE on_tv")
    suspend fun countOnAirShow(): Int

    @Query("UPDATE TvShowOnTvEntity SET on_tv = 0")
    suspend fun unMarkOnAirShow()

    @Query("SELECT tvShow.* FROM TvShowEntity tvShow INNER JOIN TvShowOnTvEntity onTv ON tvShow.id = onTv.idTvShow WHERE onTv.on_tv ORDER BY popularity DESC LIMIT :pageSize OFFSET :offset")
    suspend fun findOnAirByPopular(pageSize: Int, offset: Int): List<TvShowEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOnAirShow(entities: List<TvShowOnTvEntity>)
}