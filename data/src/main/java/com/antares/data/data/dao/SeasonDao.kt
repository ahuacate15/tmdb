package com.antares.data.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.antares.data.data.entity.SeasonEntity

@Dao
interface SeasonDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSeason(entities: List<SeasonEntity>)

    @Query("SELECT * FROM SeasonEntity WHERE tv_show_id = :idShow ORDER BY season_number")
    suspend fun findByIdShow(idShow: Int) : List<SeasonEntity>

    @Query("SELECT * FROM SeasonEntity WHERE tv_show_id = :idShow AND season_number = :seasonNumber ORDER BY season_number")
    suspend fun findByIdShowAndSeasonNumber(idShow: Int, seasonNumber: Int) : SeasonEntity
}