package com.antares.data.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.antares.data.data.entity.TvShowEntity

@Dao
interface TvShowDao {

    @Query("SELECT * FROM TvShowEntity WHERE id = :id")
    suspend fun findById(id: Int): TvShowEntity

    @Query("SELECT count(0) FROM TvShowEntity")
    suspend fun countTotalShow(): Int

    @Query("SELECT * FROM TvShowEntity ORDER BY vote_average DESC LIMIT :pageSize OFFSET :offset")
    suspend fun findTopShow(pageSize: Int, offset: Int): List<TvShowEntity>

    @Query("SELECT * FROM TvShowEntity ORDER BY popularity DESC LIMIT :pageSize OFFSET :offset")
    suspend fun findByPopular(pageSize: Int, offset: Int): List<TvShowEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertShow(entities: List<TvShowEntity>)

}