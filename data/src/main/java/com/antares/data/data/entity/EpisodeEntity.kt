package com.antares.data.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.antares.data.utils.DomainEntity
import com.antares.domain.model.Episode

@Entity
class EpisodeEntity(

    @PrimaryKey
    val id: Int,

    @ColumnInfo(name = "id_tv_show")
    val idTvShow: Int,

    @ColumnInfo(name = "name")
    val name: String,

    @ColumnInfo(name = "overview")
    val overview: String,

    @ColumnInfo(name = "episode_number")
    val episodeNumber: Int,

    @ColumnInfo(name = "season_number")
    val seasonNumber: Int,

    @ColumnInfo(name = "still_path")
    val stillPath: String?
) : DomainEntity<Episode> {

    override fun mapToDomain() = Episode(
        id = id,
        name = name,
        overview = overview,
        episodeNumber = episodeNumber,
        stillPath = stillPath
    )

}