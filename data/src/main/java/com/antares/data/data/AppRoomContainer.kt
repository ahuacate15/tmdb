package com.antares.data.data

import android.content.Context
import androidx.room.Room

class AppRoomContainer(context: Context) {

    val appRoomDB = Room.databaseBuilder(
        context,
        AppRoomDB::class.java,
        AppRoomDB.DB_NAME
    ).fallbackToDestructiveMigration()
        .build()
}