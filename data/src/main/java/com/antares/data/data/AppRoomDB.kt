package com.antares.data.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.antares.data.data.dao.*
import com.antares.data.data.entity.*

@Database(
    entities = [
        TvShowEntity::class, SeasonEntity::class, EpisodeEntity::class, AccountEntity::class,
        TvShowFavoriteEntity::class, TvShowOnTvEntity::class, TvShowAiringTodayEntity::class
    ],
    version = 17
)
abstract class AppRoomDB : RoomDatabase() {

    abstract fun TvShowDao(): TvShowDao
    abstract fun SeasonDao(): SeasonDao
    abstract fun EpisodeDao(): EpisodeDao
    abstract fun AccountDao(): AccountDao
    abstract fun TvShowOnAirDao(): TvShowOnAirDao
    abstract fun TvShowAiringTodayDao(): TvShowAiringTodayDao

    companion object {
        const val DB_NAME = "app_database"
    }
}