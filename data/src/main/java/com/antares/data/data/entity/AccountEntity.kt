package com.antares.data.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.antares.data.utils.DomainEntity
import com.antares.domain.model.auth.Account

@Entity
class AccountEntity(

    @PrimaryKey
    val id: Int,

    @ColumnInfo(name = "session_id")
    val sessionId: String,

    @ColumnInfo(name = "name")
    val name: String,

    @ColumnInfo(name = "username")
    val username: String,

    @ColumnInfo(name = "avatar")
    val avatar: String?
) : DomainEntity<Account> {

    override fun mapToDomain() = Account(
        id = id,
        name = name,
        username = username,
        avatar = avatar
    )

}