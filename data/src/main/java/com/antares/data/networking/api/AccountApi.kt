package com.antares.data.networking.api

import com.antares.data.networking.request.MarkAsFavoriteRequest
import com.antares.data.networking.response.GenericResponse
import com.antares.data.networking.response.ResultTvShowResponse
import com.antares.data.networking.response.auth.AccountResponse
import retrofit2.http.*

interface AccountApi {

    @GET("account")
    suspend fun getAccount(@Query("session_id") sessionId: String): AccountResponse

    @Headers("Content-Type: application/json")
    @POST("account/{account_id}/favorite")
    suspend fun markTvShowAsFavorite(
        @Path("account_id") accountId: Int,
        @Query("session_id") sessionId: String,
        @Body markAsFavoriteRequest: MarkAsFavoriteRequest
    ): GenericResponse

    @GET("account/{account_id}/favorite/tv")
    suspend fun getFavoriteTvShow(
        @Path("account_id") accountId: Int,
        @Query("session_id") sessionId: String,
    ): ResultTvShowResponse
}