package com.antares.data.networking.response

import com.antares.data.data.entity.EpisodeEntity
import com.antares.data.utils.DomainEntity
import com.antares.domain.model.Episode
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class EpisodeResponse(
    @Expose
    @SerializedName("id")
    val id: Int,

    @Expose
    @SerializedName("name")
    val name: String,

    @Expose
    @SerializedName("overview")
    val overview: String,

    @Expose
    @SerializedName("episode_number")
    val episodeNumber: Int,

    @Expose
    @SerializedName("still_path")
    val stillPath: String?
) : DomainEntity<Episode> {

    override fun mapToDomain() = Episode(
        id = id,
        name = name,
        overview = overview,
        episodeNumber = episodeNumber,
        stillPath = stillPath
    )

    fun mapToEntity(idTvShow: Int, seasonNumber: Int) = EpisodeEntity(
        id = id,
        idTvShow = idTvShow,
        name = name,
        overview = overview,
        episodeNumber = episodeNumber,
        seasonNumber = seasonNumber,
        stillPath = stillPath
    )

}