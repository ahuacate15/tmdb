package com.antares.data.networking.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ErrorResponse(
    @Expose
    @SerializedName("status_code")
    val statusCode: Int,

    @Expose
    @SerializedName("status_message")
    val statusMessage: String,

    @Expose
    @SerializedName("success")
    val success: Boolean
)