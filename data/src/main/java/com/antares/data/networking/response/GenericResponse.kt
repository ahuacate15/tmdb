package com.antares.data.networking.response

import com.antares.data.utils.DomainEntity
import com.antares.domain.model.GenericDomainResponse
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GenericResponse(
    @Expose
    @SerializedName("status_code")
    val statusCode: Int,

    @Expose
    @SerializedName("status_message")
    val statusMessage: String
) : DomainEntity<GenericDomainResponse> {

    override fun mapToDomain() = GenericDomainResponse(
        statusCode = statusCode
    )

}