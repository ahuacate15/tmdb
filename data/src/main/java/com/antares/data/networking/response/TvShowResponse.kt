package com.antares.data.networking.response

import com.antares.data.data.entity.TvShowEntity
import com.antares.domain.model.TvShow
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class TvShowResponse(
    @Expose
    @SerializedName("id")
    val id: Int,

    @Expose
    @SerializedName("name")
    val name: String,

    @Expose
    @SerializedName("original_name")
    val originalName: String,

    @Expose
    @SerializedName("poster_path")
    val posterPath: String,

    @Expose
    @SerializedName("vote_average")
    val voteAverage: Float,

    @Expose
    @SerializedName("popularity")
    val popularity: Float,

    @Expose
    @SerializedName("overview")
    val overview: String,

    @Expose
    @SerializedName("seasons")
    val seasons: List<SeasonResponse>?
) {

    fun mapToDomain(favorite: Boolean = false): TvShow =
        TvShow(
            id = id,
            name = name,
            originalName = originalName,
            posterPath = posterPath,
            voteAverage = voteAverage,
            popularity = popularity,
            overview = overview,
            favorite = favorite,
            seasons = seasons?.map { it.mapToDomain() }
        )

    fun mapToEntity() = TvShowEntity(
        id = id,
        name = name,
        originalName = originalName,
        posterPath = posterPath,
        voteAverage = voteAverage,
        popularity = popularity,
        overview = overview
    )

}