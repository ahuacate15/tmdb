package com.antares.data.networking.api

import com.antares.data.networking.request.DeleteSessionRequest
import com.antares.data.networking.request.RequestToken
import com.antares.data.networking.response.GeneratedSessionResponse
import com.antares.data.networking.response.TmpTokenResponse
import com.antares.data.networking.response.auth.DeleteSessionResponse
import retrofit2.http.*

interface AuthApi {

    @GET("authentication/token/new")
    suspend fun createTemporaryToken(): TmpTokenResponse

    @Headers("Content-Type: application/json")
    @POST("authentication/session/new")
    suspend fun createNewSession(@Body requestToken: RequestToken): GeneratedSessionResponse

    @HTTP(method = "DELETE", path = "authentication/session", hasBody = true)
    suspend fun deleteSession(@Body deleteSessionRequest: DeleteSessionRequest): DeleteSessionResponse

}