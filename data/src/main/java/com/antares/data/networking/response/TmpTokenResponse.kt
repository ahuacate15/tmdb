package com.antares.data.networking.response

import com.antares.data.utils.DomainEntity
import com.antares.domain.model.auth.TmpToken
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TmpTokenResponse(
    @Expose
    @SerializedName("success")
    val success: Boolean,

    @Expose
    @SerializedName("request_token")
    val requestToken: String
) : DomainEntity<TmpToken> {

    override fun mapToDomain(): TmpToken = TmpToken(success, requestToken)
}