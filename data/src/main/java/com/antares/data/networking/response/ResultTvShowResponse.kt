package com.antares.data.networking.response

import com.antares.data.utils.DomainEntity
import com.antares.domain.model.ResultTvShow
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ResultTvShowResponse(
    @Expose
    @SerializedName("page")
    val page: Int,

    @Expose
    @SerializedName("total_pages")
    val totalPages: Int,

    @Expose
    @SerializedName("total_results")
    val totalResults: Int,

    @Expose
    @SerializedName("results")
    val results: List<TvShowResponse>
) : DomainEntity<ResultTvShow> {

    override fun mapToDomain(): ResultTvShow =
        ResultTvShow(page, totalPages, totalResults, results.map { it.mapToDomain() })
}