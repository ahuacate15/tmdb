package com.antares.data.networking.api

import com.antares.data.networking.response.ResultTvShowResponse
import com.antares.data.networking.response.TvShowResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TvShowApi {

    @GET("tv/{id}")
    suspend fun findById(@Path("id") id: Int): TvShowResponse

    @GET("tv/top_rated")
    suspend fun findTopShow(@Query("page") page: Int? = 1): ResultTvShowResponse

    @GET("tv/popular")
    suspend fun findByPopularShow(@Query("page") page: Int? = 1): ResultTvShowResponse

    @GET("tv/on_the_air")
    suspend fun findOnTheAirShow(@Query("page") page: Int? = 1): ResultTvShowResponse

    @GET("tv/airing_today")
    suspend fun findOnAiringTodayShow(@Query("page") page: Int? = 1): ResultTvShowResponse

}