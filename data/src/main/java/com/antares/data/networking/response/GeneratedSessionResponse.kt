package com.antares.data.networking.response

import com.antares.data.utils.DomainEntity
import com.antares.domain.model.auth.GeneratedSession
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GeneratedSessionResponse(

    @Expose
    @SerializedName("success")
    val success: Boolean,

    @Expose
    @SerializedName("session_id")
    val sessionId: String
) : DomainEntity<GeneratedSession> {

    override fun mapToDomain(): GeneratedSession =
        GeneratedSession(success = success, sessionId = sessionId)
}