package com.antares.data.networking.response.auth

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AvatarResponse(
    @Expose
    @SerializedName("tmdb")
    val tmdbResponse: TmdbResponse
)