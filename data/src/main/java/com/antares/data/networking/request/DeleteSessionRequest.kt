package com.antares.data.networking.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DeleteSessionRequest(
    @Expose
    @SerializedName("session_id")
    val sessionId: String
)