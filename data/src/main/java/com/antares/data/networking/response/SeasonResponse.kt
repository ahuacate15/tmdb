package com.antares.data.networking.response

import com.antares.data.data.entity.SeasonEntity
import com.antares.data.utils.DomainEntity
import com.antares.domain.model.Season
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class SeasonResponse(
    @Expose
    @SerializedName("id")
    val id: Int,

    @Expose
    @SerializedName("name")
    val name: String,

    @Expose
    @SerializedName("poster_path")
    val posterPath: String?,

    @Expose
    @SerializedName("overview")
    val overview: String,

    @Expose
    @SerializedName("season_number")
    val seasonNumber: Int,

    @Expose
    @SerializedName("episode_count")
    val episodeCount: Int,

    @Expose
    @SerializedName("episodes")
    val episodes: List<EpisodeResponse>?
) : DomainEntity<Season> {

    fun mapToEntity(tvShowId: Int) = SeasonEntity(
        id = id,
        tvShowId = tvShowId,
        name = name,
        posterPath = posterPath,
        overview = overview,
        seasonNumber = seasonNumber,
        episodeCount = episodeCount
    )

    override fun mapToDomain() = Season(
        id = id,
        name = name,
        posterPath = posterPath,
        overview = overview,
        seasonNumber = seasonNumber,
        episodeCount = episodeCount,
        episodes = episodes?.map { it.mapToDomain() }
    )

}