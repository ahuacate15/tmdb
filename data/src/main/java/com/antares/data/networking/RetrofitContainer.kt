package com.antares.data.networking

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Singleton
class RetrofitContainer {

    val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(getCustomHttpClient())
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private fun getCustomHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor { chain ->
                val url =
                    chain.request().url().newBuilder().addQueryParameter("api_key", API_KEY).build()
                chain.proceed(chain.request().newBuilder().url(url).build())
            }.build()
    }

    companion object {
        private const val BASE_URL = "https://api.themoviedb.org/3/"
        private const val API_KEY = "d654cf7d2c7c5ff4b3bfb17fc78bc8da"
    }
}