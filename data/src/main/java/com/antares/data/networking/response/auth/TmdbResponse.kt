package com.antares.data.networking.response.auth

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TmdbResponse (
    @Expose
    @SerializedName("avatar_path")
    val avatarPath: String
)