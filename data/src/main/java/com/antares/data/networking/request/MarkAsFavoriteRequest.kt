package com.antares.data.networking.request

import com.google.gson.annotations.SerializedName

data class MarkAsFavoriteRequest(
    @SerializedName("media_type")
    val mediaType: String = "tv",

    @SerializedName("media_id")
    val mediaId: Int,

    @SerializedName("favorite")
    val favorite: Boolean
)