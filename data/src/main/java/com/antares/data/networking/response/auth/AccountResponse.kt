package com.antares.data.networking.response.auth

import com.antares.data.data.entity.AccountEntity
import com.antares.data.utils.DomainEntity
import com.antares.domain.model.auth.Account
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AccountResponse(
    @Expose
    @SerializedName("id")
    val id: Int,

    @Expose
    @SerializedName("name")
    val name: String,

    @Expose
    @SerializedName("username")
    val username: String,

    @Expose
    @SerializedName("avatar")
    val avatar: AvatarResponse
) : DomainEntity<Account> {

    override fun mapToDomain() = Account(
        id = id,
        name = name,
        username = username,
        avatar = avatar.tmdbResponse.avatarPath
    )

    fun mapToEntity(sessionId: String) = AccountEntity(
        id = id,
        sessionId = sessionId,
        name = name,
        username = username,
        avatar = avatar.tmdbResponse.avatarPath
    )

}