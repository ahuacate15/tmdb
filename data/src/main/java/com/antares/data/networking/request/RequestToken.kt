package com.antares.data.networking.request

import com.google.gson.annotations.SerializedName

class RequestToken(
    @SerializedName("request_token")
    val requestToken: String
)