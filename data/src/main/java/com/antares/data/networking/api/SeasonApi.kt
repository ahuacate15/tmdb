package com.antares.data.networking.api

import com.antares.data.networking.response.SeasonResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface SeasonApi {

    @GET("tv/{idTvShow}/season/{seasonNumber}")
    suspend fun findByIdShowAndSeasonNumber(
        @Path("idTvShow") idTvShow: Int,
        @Path("seasonNumber") seasonNumber: Int
    ): SeasonResponse
}