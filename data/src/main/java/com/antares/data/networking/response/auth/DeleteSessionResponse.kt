package com.antares.data.networking.response.auth

import com.antares.data.utils.DomainEntity
import com.antares.domain.model.auth.DeleteSession
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DeleteSessionResponse(
    @Expose
    @SerializedName("success")
    val success: Boolean
): DomainEntity<DeleteSession> {

    override fun mapToDomain() = DeleteSession(success)

}