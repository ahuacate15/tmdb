package com.antares.data.repository

import com.antares.data.data.dao.AccountDao
import com.antares.data.data.entity.AccountEntity
import com.antares.data.data.entity.TvShowEntity
import com.antares.data.data.entity.TvShowFavoriteEntity

class FakeAccountDao(
    private val list: MutableList<AccountEntity> = mutableListOf(),
    private val listTvShow: MutableList<TvShowEntity> = mutableListOf()
) : AccountDao {

    private val listFavorite: MutableList<TvShowFavoriteEntity> = mutableListOf()

    override suspend fun insertAccount(account: AccountEntity) {
        list.add(account)
    }

    override suspend fun findBySessionId(sessionId: String) = list.first {
        it.sessionId == sessionId
    }

    override suspend fun findFavorite(): List<TvShowEntity> = listTvShow

    override suspend fun maskShowAsFavorite(entity: TvShowFavoriteEntity) {
        listFavorite.add(entity)
    }

    override suspend fun verifyFavoriteShow(idTvShow: Int) = listFavorite.first {
        it.idTvShow == idTvShow
    }
}