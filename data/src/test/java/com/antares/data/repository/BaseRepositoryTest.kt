package com.antares.data.repository

import com.antares.data.coroutine.TestCoroutineContextProvider
import com.antares.data.utils.Connectivity
import com.antares.domain.model.ResultWrapper
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.lang.Exception

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class BaseRepositoryTest {

    @Mock
    private lateinit var connectivity: Connectivity

    private lateinit var baseRepository: BaseRepository

    @Before
    fun setup() {
        baseRepository = BaseRepository(connectivity, TestCoroutineContextProvider())
    }

    @Test
    fun `test fetchData when network is up`() = runBlockingTest {
        //given network access
        `when`(connectivity.hasNetworkAccess())
            .thenReturn(true)

        val fetchData = baseRepository.fetchData(
            apiDataProvider = { "data from api" },
            dbDataProvider = { "data from cache" }
        )

        //then verify the network data is fetched
        assertTrue(fetchData is ResultWrapper.Success)
        assertEquals("data from api", (fetchData as ResultWrapper.Success).data)
    }

    @Test
    fun `test fetchData when network is down`() = runBlockingTest {
        //given the network disabled
        `when`(connectivity.hasNetworkAccess())
            .thenReturn(false)


        val fetchData = baseRepository.fetchData(
            apiDataProvider = { "data from api" },
            dbDataProvider = { "data from cache" }
        )

        //then verify the cache data is fetched
        assertTrue(fetchData is ResultWrapper.Success)
        assertEquals("data from cache", (fetchData as ResultWrapper.Success).data)
    }

    @Test
    fun `test fetchData verify IOException is parsed safely as networkError`() = runBlockingTest {
        //given network access
        `when`(connectivity.hasNetworkAccess())
            .thenReturn(true)

        val fetchData = baseRepository.fetchData(
            apiDataProvider = { throw IOException("") },
            dbDataProvider = { "data from cache" }
        )

        //then verify the type of error
        assertTrue(fetchData is ResultWrapper.NetworkError)
    }

    @Test
    fun `test fetchData verify HttpException is parsed safe as generic error, mapping error messages`() =
        runBlockingTest {
            //given network access
            `when`(connectivity.hasNetworkAccess())
                .thenReturn(true)

            val fetchData = baseRepository.fetchData(
                apiDataProvider = {
                    throw HttpException(
                        Response.error<Any>(
                            400, ResponseBody.create(
                                MediaType.parse("application/json"),
                                "{ \"status_message\" : \"mock error\", \"status_code\" : 10 }"
                            )
                        )
                    )
                },
                dbDataProvider = { "data from cache" }
            )

            //then verify the type and content of error
            assertTrue(fetchData is ResultWrapper.GenericError)
            assertEquals("mock error", (fetchData as ResultWrapper.GenericError).message)
            assertEquals(10, fetchData.code)
        }

    @Test
    fun `test fetchData verify generic error`() = runBlockingTest {
        //given network access
        `when`(connectivity.hasNetworkAccess())
            .thenReturn(true)

        val fetchData = baseRepository.fetchData(
            apiDataProvider = { throw Exception("generic exception") },
            dbDataProvider = { "data from cache" }
        )

        //then verify the type of error
        assertTrue(fetchData is ResultWrapper.GenericError)
        assertNull((fetchData as ResultWrapper.GenericError).message)
        assertNull(fetchData.code)
    }
}