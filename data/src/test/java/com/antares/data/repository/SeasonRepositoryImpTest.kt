package com.antares.data.repository

import com.antares.data.coroutine.TestCoroutineContextProvider
import com.antares.data.data.dao.EpisodeDao
import com.antares.data.data.dao.SeasonDao
import com.antares.data.data.entity.EpisodeEntity
import com.antares.data.data.entity.SeasonEntity
import com.antares.data.networking.api.SeasonApi
import com.antares.data.networking.response.SeasonResponse
import com.antares.data.utils.Connectivity
import com.antares.domain.model.ResultWrapper
import com.antares.domain.repository.SeasonRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.anyList
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class SeasonRepositoryImpTest {

    @Mock
    private lateinit var seasonApi: SeasonApi

    @Mock
    private lateinit var seasonDao: SeasonDao

    @Mock
    private lateinit var episodeDao: EpisodeDao

    @Mock
    private lateinit var connectivity: Connectivity

    private lateinit var seasonRepository: SeasonRepository

    @Before
    fun setup() {
        seasonRepository = SeasonRepositoryImp(
            seasonApi = seasonApi,
            seasonDao = seasonDao,
            episodeDao = episodeDao,
            connectivity = connectivity,
            dispatcher = TestCoroutineContextProvider()
        )
    }

    @Test
    fun `test findByIdShowAndSeasonNumber when network is up`() = runBlockingTest {
        val idTvShow = 1
        val idSeason = 100
        val seasonNumber = 1

        //given network access
        `when`(connectivity.hasNetworkAccess())
            .thenReturn(true)

        `when`(seasonApi.findByIdShowAndSeasonNumber(idTvShow, seasonNumber))
            .thenReturn(
                SeasonResponse(
                    id = idSeason,
                    name = "overture season",
                    posterPath = "fake_path",
                    overview = "overview",
                    seasonNumber = seasonNumber,
                    episodeCount = 1,
                    episodes = listOf()
                )
            )

        `when`(seasonDao.insertSeason(anyList()))
            .then { it.mock }
        `when`(episodeDao.insertEpisode(anyList()))
            .then { it.mock }

        //when fetch season by idShow and seasonNumber
        val response = seasonRepository.findByIdShowAndSeasonNumber(idTvShow, seasonNumber)

        //then verify the response and the data is matched
        assertTrue(response is ResultWrapper.Success)
        assertEquals(idSeason, (response as ResultWrapper.Success).data.id)
        assertEquals("overture season", response.data.name)
        assertEquals("fake_path", response.data.posterPath)
        assertEquals("overview", response.data.overview)
        assertEquals(seasonNumber, response.data.seasonNumber)
        assertEquals(1, response.data.episodeCount)
    }

    @Test
    fun `test findByIdShowAndSeasonNumber when network is down`() = runBlockingTest {
        val idTvShow = 1
        val idSeason = 100
        val seasonNumber = 1

        //given network access
        `when`(connectivity.hasNetworkAccess())
            .thenReturn(false)

        `when`(seasonDao.findByIdShowAndSeasonNumber(idTvShow, seasonNumber))
            .thenReturn(
                SeasonEntity(
                    id = idSeason,
                    tvShowId = idTvShow,
                    name = "overture season",
                    posterPath = "fake_path",
                    overview = "overview",
                    seasonNumber = seasonNumber,
                    episodeCount = 10
                )
            )

        `when`(episodeDao.findByIdShowAndSeasonNumber(idTvShow, seasonNumber))
            .thenReturn(listOf(
                EpisodeEntity(
                    id = 1,
                    idTvShow = idTvShow,
                    name = "episode 1",
                    overview = "episode overview",
                    episodeNumber = 1,
                    seasonNumber = seasonNumber,
                    stillPath = "fake_path"
                )
            ))

        //when season is fetched by idShow and seasonNumber
        val response = seasonRepository.findByIdShowAndSeasonNumber(idTvShow, seasonNumber)

        //then verify the response is success
        assertTrue(response is ResultWrapper.Success)
        assertEquals(idSeason, (response as ResultWrapper.Success).data.id)
        assertEquals("overture season", response.data.name)
        assertEquals("fake_path", response.data.posterPath)
        assertEquals("overview", response.data.overview)
        assertEquals(seasonNumber, response.data.seasonNumber)
        assertEquals(10, response.data.episodeCount)

        //then verify also, the list of episodes
        assertEquals(1, response.data.episodes?.size)
        assertEquals(1, response.data.episodes?.first()?.id)
        assertEquals("episode 1", response.data.episodes?.first()?.name)
        assertEquals("episode overview", response.data.episodes?.first()?.overview)
        assertEquals(1, response.data.episodes?.first()?.episodeNumber)
        assertEquals("fake_path", response.data.episodes?.first()?.stillPath)
    }
}