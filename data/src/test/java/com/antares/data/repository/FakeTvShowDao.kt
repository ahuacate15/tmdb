package com.antares.data.repository

import com.antares.data.data.dao.TvShowDao
import com.antares.data.data.entity.TvShowEntity

class FakeTvShowDao(
    private val list: MutableList<TvShowEntity> = mutableListOf()
) : TvShowDao {

    override suspend fun findById(id: Int) = list.first {
        it.id == id
    }

    override suspend fun countTotalShow() = list.size

    override suspend fun findTopShow(pageSize: Int, offset: Int) = list.sortedByDescending {
        it.voteAverage
    }

    override suspend fun findByPopular(pageSize: Int, offset: Int) = list.sortedByDescending {
        it.popularity
    }

    override suspend fun insertShow(entities: List<TvShowEntity>) {
        list.addAll(entities)
    }
}