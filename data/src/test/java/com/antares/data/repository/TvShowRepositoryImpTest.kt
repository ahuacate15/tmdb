package com.antares.data.repository

import com.antares.data.coroutine.TestCoroutineContextProvider
import com.antares.data.data.dao.*
import com.antares.data.data.entity.SeasonEntity
import com.antares.data.data.entity.TvShowEntity
import com.antares.data.networking.api.TvShowApi
import com.antares.data.networking.response.ResultTvShowResponse
import com.antares.data.networking.response.TvShowResponse
import com.antares.data.utils.Connectivity
import com.antares.domain.model.ResultWrapper
import com.antares.domain.repository.TvShowRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.anyList
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class TvShowRepositoryImpTest {

    @Mock
    private lateinit var tvShowApi: TvShowApi

    @Mock
    private lateinit var tvShowOrAirDao: TvShowOnAirDao

    @Mock
    private lateinit var tvShowAiringTodayDao: TvShowAiringTodayDao

    @Mock
    private lateinit var seasonDao: SeasonDao

    @Mock
    private lateinit var accountDao: AccountDao

    @Mock
    private lateinit var connectivity: Connectivity

    private lateinit var tvShowDao: TvShowDao
    private lateinit var tvShowRepository: TvShowRepository

    @Before
    fun setup() {
        val listTvShow = mutableListOf(
            TvShowEntity(
                id = 100,
                name = "spongebob",
                originalName = "spongebob",
                posterPath = "fake_path",
                voteAverage = 55f,
                popularity = 10f,
                overview = "SpongeBob SquarePants is an energetic and optimistic sea sponge who lives in a submerged pineapple."
            ),
            TvShowEntity(
                id = 300,
                name = "pickles",
                originalName = "pickles",
                posterPath = "fake_path",
                voteAverage = 20f,
                popularity = 30f,
                overview = "was a black and white collie dog"
            ),
        )
        tvShowDao = FakeTvShowDao(listTvShow)
        tvShowRepository = TvShowRepositoryImp(
            tvShowApi = tvShowApi,
            tvShowDao = tvShowDao,
            tvShowOnAirDao = tvShowOrAirDao,
            tvShowAiringTodayDao = tvShowAiringTodayDao,
            seasonDao = seasonDao,
            accountDao = accountDao,
            connectivity = connectivity,
            dispatcher = TestCoroutineContextProvider()
        )
    }

    @Test
    fun `test findTopTVShow when internet is up`() = runBlockingTest {
        //given network access and one row returned
        `when`(connectivity.hasNetworkAccess())
            .thenReturn(true)

        `when`(tvShowApi.findTopShow(page = 1))
            .thenReturn(
                ResultTvShowResponse(
                    page = 1,
                    totalPages = 1,
                    totalResults = 1,
                    results = listOf(
                        TvShowResponse(
                            id = 100,
                            name = "spongebob",
                            originalName = "spongebob",
                            posterPath = "fake_path",
                            voteAverage = 55f,
                            popularity = 0f,
                            overview = "SpongeBob SquarePants is an energetic and optimistic sea sponge who lives in a submerged pineapple.",
                            seasons = null
                        )
                    )
                )
            )

        //when network data is fetched
        val response = tvShowRepository.findTopTVShow(page = 1)

        //then verify the fetched data and the first is top
        assertTrue(response is ResultWrapper.Success)
        assertEquals(1, (response as ResultWrapper.Success).data.page)
        assertEquals(1, response.data.totalPages)
        assertEquals(1, response.data.totalResults)
        assertTrue(response.data.results.isNotEmpty())

        //then also, verify the detail of tvShow fetched
        assertEquals(100, response.data.results.first().id)
        assertEquals("spongebob", response.data.results.first().name)
        assertEquals("spongebob", response.data.results.first().originalName)
        assertEquals("fake_path", response.data.results.first().posterPath)
        assertEquals(55f, response.data.results.first().voteAverage)
        assertEquals(0f, response.data.results.first().popularity)
        assertEquals(
            "SpongeBob SquarePants is an energetic and optimistic sea sponge who lives in a submerged pineapple.",
            response.data.results.first().overview
        )
        assertTrue(response.data.results.first().seasons.isNullOrEmpty())
    }

    @Test
    fun `test findTopTVShow when internet is down`() = runBlockingTest {
        val firstPage = 1

        //given network disabled
        `when`(connectivity.hasNetworkAccess())
            .thenReturn(false)

        //when cache data is fetched
        val response = tvShowRepository.findTopTVShow(page = firstPage)

        //then verify the fetch data
        assertTrue(response is ResultWrapper.Success)
        assertEquals(firstPage, (response as ResultWrapper.Success).data.page)
        assertEquals(1, response.data.totalPages)
        assertEquals(2, response.data.totalResults)
        assertEquals(2, response.data.results.size)

        //then also, verify the first row is the top
        assertEquals(100, response.data.results.first().id)
        assertEquals("spongebob", response.data.results.first().name)
    }

    @Test
    fun `test findByPopularTVShow when internet is up`() = runBlockingTest {
        val firstPage = 1

        //given network access
        `when`(connectivity.hasNetworkAccess())
            .thenReturn(true)

        `when`(tvShowApi.findByPopularShow(page = firstPage))
            .thenReturn(
                ResultTvShowResponse(
                    page = 1,
                    totalPages = 1,
                    totalResults = 1,
                    results = listOf(
                        TvShowResponse(
                            id = 100,
                            name = "spongebob",
                            originalName = "spongebob",
                            posterPath = "fake_path",
                            voteAverage = 55f,
                            popularity = 0f,
                            overview = "SpongeBob SquarePants is an energetic and optimistic sea sponge who lives in a submerged pineapple.",
                            seasons = null
                        )
                    )
                )
            )

        //when network data is fetched
        val response = tvShowRepository.findByPopularTVShow(page = 1)

        //then verify the fetched data and the first is top
        assertTrue(response is ResultWrapper.Success)
        assertEquals(1, (response as ResultWrapper.Success).data.page)
        assertEquals(1, response.data.totalPages)
        assertEquals(1, response.data.totalResults)
        assertTrue(response.data.results.isNotEmpty())

        //then also, verify the detail of tvShow fetched
        assertEquals(100, response.data.results.first().id)
        assertEquals("spongebob", response.data.results.first().name)
        assertEquals("spongebob", response.data.results.first().originalName)
        assertEquals("fake_path", response.data.results.first().posterPath)
        assertEquals(55f, response.data.results.first().voteAverage)
        assertEquals(0f, response.data.results.first().popularity)
        assertEquals(
            "SpongeBob SquarePants is an energetic and optimistic sea sponge who lives in a submerged pineapple.",
            response.data.results.first().overview
        )
        assertTrue(response.data.results.first().seasons.isNullOrEmpty())
    }

    @Test
    fun `test findByPopularTVShow when internet is down`() = runBlockingTest {
        val firstPage = 1

        //given network disabled
        `when`(connectivity.hasNetworkAccess())
            .thenReturn(false)

        //when cache data is fetched
        val response = tvShowRepository.findByPopularTVShow(page = firstPage)

        //then verify the fetch data
        assertTrue(response is ResultWrapper.Success)
        assertEquals(firstPage, (response as ResultWrapper.Success).data.page)
        assertEquals(1, response.data.totalPages)
        assertEquals(2, response.data.totalResults)
        assertEquals(2, response.data.results.size)

        //then also, verify the first row is the most popular
        assertEquals(300, response.data.results.first().id)
        assertEquals("pickles", response.data.results.first().name)
    }

    @Test
    fun `test findOnTheAirShowTvShow when internet is up`() = runBlockingTest {
        val firstPage = 1

        //given network access
        `when`(connectivity.hasNetworkAccess())
            .thenReturn(true)

        `when`(tvShowApi.findOnTheAirShow(firstPage))
            .thenReturn(
                ResultTvShowResponse(
                    page = 1,
                    totalPages = 1,
                    totalResults = 1,
                    results = listOf(
                        TvShowResponse(
                            id = 100,
                            name = "spongebob",
                            originalName = "spongebob",
                            posterPath = "fake_path",
                            voteAverage = 55f,
                            popularity = 0f,
                            overview = "SpongeBob SquarePants is an energetic and optimistic sea sponge who lives in a submerged pineapple.",
                            seasons = null
                        )
                    )
                )
            )
        `when`(tvShowOrAirDao.unMarkOnAirShow())
            .then { it.mock }
        `when`(tvShowOrAirDao.insertOnAirShow(anyList()))
            .then { it.mock }


        //when network data is fetched
        val response = tvShowRepository.findOnTheAirShowTvShow(firstPage)

        //then verify the fetched data
        assertTrue(response is ResultWrapper.Success)
        assertEquals(1, (response as ResultWrapper.Success).data.page)
        assertEquals(1, response.data.totalPages)
        assertEquals(1, response.data.totalResults)
        assertTrue(response.data.results.isNotEmpty())
    }

    @Test
    fun `test findOnTheAirShowTvShow when internet is down`() = runBlockingTest {
        val firstPage = 1

        //given network disabled
        `when`(connectivity.hasNetworkAccess())
            .thenReturn(false)
        `when`(tvShowOrAirDao.countOnAirShow())
            .thenReturn(1)
        `when`(tvShowOrAirDao.findOnAirByPopular(pageSize = 20, offset = 0))
            .thenReturn(
                listOf(
                    TvShowEntity(
                        id = 100,
                        name = "spongebob",
                        originalName = "spongebob",
                        posterPath = "fake_path",
                        voteAverage = 55f,
                        popularity = 0f,
                        overview = "SpongeBob SquarePants is an energetic and optimistic sea sponge who lives in a submerged pineapple."
                    )
                )
            )

        //when cache data is fetched
        val response = tvShowRepository.findOnTheAirShowTvShow(firstPage)

        //then verify the fetched data
        assertTrue(response is ResultWrapper.Success)
        assertEquals(firstPage, (response as ResultWrapper.Success).data.page)
        assertEquals(1, response.data.totalPages)
        assertEquals(1, response.data.totalResults)
    }

    @Test
    fun `test findOnAiringTodayTVShow when internet is up`() = runBlockingTest {
        val firstPage = 1

        //given network access
        `when`(connectivity.hasNetworkAccess())
            .thenReturn(true)

        `when`(tvShowApi.findOnAiringTodayShow(firstPage))
            .thenReturn(
                ResultTvShowResponse(
                    page = 1,
                    totalPages = 1,
                    totalResults = 1,
                    results = listOf(
                        TvShowResponse(
                            id = 100,
                            name = "spongebob",
                            originalName = "spongebob",
                            posterPath = "fake_path",
                            voteAverage = 55f,
                            popularity = 0f,
                            overview = "SpongeBob SquarePants is an energetic and optimistic sea sponge who lives in a submerged pineapple.",
                            seasons = null
                        )
                    )
                )
            )
        `when`(tvShowAiringTodayDao.unMarkAiringTodayShow())
            .then { it.mock }
        `when`(tvShowAiringTodayDao.insertAiringTodayShow(anyList()))
            .then { it.mock }


        //when network data is fetched
        val response = tvShowRepository.findOnAiringTodayTVShow(firstPage)

        //then verify the fetched data
        assertTrue(response is ResultWrapper.Success)
        assertEquals(1, (response as ResultWrapper.Success).data.page)
        assertEquals(1, response.data.totalPages)
        assertEquals(1, response.data.totalResults)
        assertTrue(response.data.results.isNotEmpty())
    }

    @Test
    fun `test findOnAiringTodayTVShow when internet is down`() = runBlockingTest {
        val firstPage = 1

        //given network disabled
        `when`(connectivity.hasNetworkAccess())
            .thenReturn(false)
        `when`(tvShowAiringTodayDao.countAiringTodayShow())
            .thenReturn(1)
        `when`(tvShowAiringTodayDao.findAiringTodayByPopular(pageSize = 20, offset = 0))
            .thenReturn(
                listOf(
                    TvShowEntity(
                        id = 100,
                        name = "spongebob",
                        originalName = "spongebob",
                        posterPath = "fake_path",
                        voteAverage = 55f,
                        popularity = 0f,
                        overview = "SpongeBob SquarePants is an energetic and optimistic sea sponge who lives in a submerged pineapple."
                    )
                )
            )

        //when cache data is fetched from cache
        val response = tvShowRepository.findOnAiringTodayTVShow(firstPage)

        //then verify the fetched data
        assertTrue(response is ResultWrapper.Success)
        assertEquals(firstPage, (response as ResultWrapper.Success).data.page)
        assertEquals(1, response.data.totalPages)
        assertEquals(1, response.data.totalResults)
    }

    @Test
    fun `test findTVShowById when internet is up`() = runBlockingTest {
        val idTvShow = 1

        //given network disabled
        `when`(connectivity.hasNetworkAccess())
            .thenReturn(true)

        `when`(tvShowApi.findById(idTvShow))
            .thenReturn(
                TvShowResponse(
                    id = 100,
                    name = "spongebob",
                    originalName = "spongebob",
                    posterPath = "fake_path",
                    voteAverage = 55f,
                    popularity = 0f,
                    overview = "SpongeBob SquarePants is an energetic and optimistic sea sponge who lives in a submerged pineapple.",
                    seasons = null
                )
            )

        //when cache data is fetched from internet
        val response = tvShowRepository.findTVShowById(idTvShow)

        //then verify the fetched data
        assertTrue(response is ResultWrapper.Success)
        assertEquals(100, (response as ResultWrapper.Success).data.id)
        assertFalse(response.data.favorite)
    }

    @Test
    fun `test findTVShowById when internet is down`() = runBlockingTest {
        val idTvShow = 100

        //given network disabled
        `when`(connectivity.hasNetworkAccess())
            .thenReturn(false)

        `when`(seasonDao.findByIdShow(idTvShow))
            .thenReturn(
                listOf(
                    SeasonEntity(
                        id = 1000,
                        tvShowId = idTvShow,
                        name = "season 1",
                        posterPath = "fake_path",
                        overview = "overview",
                        seasonNumber = 1,
                        episodeCount = 1
                    )
                )
            )

        `when`(accountDao.verifyFavoriteShow(idTvShow))
            .thenReturn(null)


        //when cache data is fetched from cache
        val response = tvShowRepository.findTVShowById(idTvShow)

        //then verify the fetched data
        assertTrue(response is ResultWrapper.Success)
        assertEquals(100, (response as ResultWrapper.Success).data.id)
        assertFalse(response.data.favorite)
    }
}