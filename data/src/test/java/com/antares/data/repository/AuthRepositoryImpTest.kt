package com.antares.data.repository

import com.antares.data.any
import com.antares.data.coroutine.TestCoroutineContextProvider
import com.antares.data.networking.api.AuthApi
import com.antares.data.networking.response.GeneratedSessionResponse
import com.antares.data.networking.response.TmpTokenResponse
import com.antares.data.networking.response.auth.DeleteSessionResponse
import com.antares.data.utils.Connectivity
import com.antares.domain.model.ResultWrapper
import com.antares.domain.repository.AuthRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.HttpException

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class AuthRepositoryImpTest {

    @Mock
    private lateinit var authApi: AuthApi

    @Mock
    private lateinit var connectivity: Connectivity

    private lateinit var authRepository: AuthRepository

    @Before
    fun setup() {
        authRepository = AuthRepositoryImp(authApi, connectivity, TestCoroutineContextProvider())
    }

    @Test
    fun `test createTemporaryToken`() = runBlockingTest {
        //given a temp token
        val tmpToken = TmpTokenResponse(
            success = true,
            requestToken = "123456789"
        )

        `when`(authApi.createTemporaryToken())
            .thenReturn(tmpToken)

        //then verify the fetched data match
        val response = authRepository.createTemporaryToken()
        assertTrue(response is ResultWrapper.Success)
        assertTrue((response as ResultWrapper.Success).data.success)
        assertEquals("123456789", response.data.requestToken)
    }

    @Test
    fun `test createNewSession when requestToken is valid`() = runBlockingTest {
        //given a valid requestToken
        val validToken = "aabbccdd"

        val session = GeneratedSessionResponse(
            success = true,
            sessionId = "valid_session_id"
        )

        `when`(authApi.createNewSession(any()))
            .thenReturn(session)

        //when new session is created
        val response = authRepository.createNewSession(validToken)

        //then verify response is success and data match
        assertTrue(response is ResultWrapper.Success)
        assertEquals("valid_session_id", (response as ResultWrapper.Success).data.sessionId)
        assertTrue(response.data.success)
    }

    @Test
    fun `test createNewSession when requestToken is invalid`() = runBlockingTest{
        //when api call produces a network exception
        `when`(authApi.createNewSession(any()))
            .thenThrow(HttpException::class.java)
        val response = authRepository.createNewSession("")

        //verify the response is handled correctly
        assertTrue(response is ResultWrapper.GenericError)
    }

    @Test
    fun `test deleteSession when sessionId is valid`() = runBlockingTest {
        //when api is called using a valid sessionId
        `when`(authApi.deleteSession(any()))
            .thenReturn(DeleteSessionResponse(true))

        val response = authRepository.deleteSession("")

        //then verify the response is success
        assertTrue(response is ResultWrapper.Success)
    }
}