package com.antares.data.repository

import com.antares.data.any
import com.antares.data.coroutine.TestCoroutineContextProvider
import com.antares.data.data.dao.AccountDao
import com.antares.data.data.dao.TvShowDao
import com.antares.data.data.entity.AccountEntity
import com.antares.data.data.entity.TvShowEntity
import com.antares.data.networking.api.AccountApi
import com.antares.data.networking.response.GenericResponse
import com.antares.data.networking.response.ResultTvShowResponse
import com.antares.data.networking.response.TvShowResponse
import com.antares.data.networking.response.auth.AccountResponse
import com.antares.data.networking.response.auth.AvatarResponse
import com.antares.data.networking.response.auth.TmdbResponse
import com.antares.data.utils.Connectivity
import com.antares.domain.model.ResultWrapper
import com.antares.domain.repository.AccountRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class AccountRepositoryImpTest {

    @Mock
    private lateinit var accountApi: AccountApi

    @Mock
    private lateinit var connectivity: Connectivity

    /**
     * this test require a lot of interaction with cache data
     * fakeDao is easier to use than mockDao for this proposal
     */
    private lateinit var tvShowDao: TvShowDao
    private lateinit var accountDao: AccountDao

    private lateinit var accountRepository: AccountRepository

    @Before
    fun setup() {
        accountDao = FakeAccountDao(mutableListOf())
        tvShowDao = FakeTvShowDao(mutableListOf())
        accountRepository = AccountRepositoryImp(
            accountApi = accountApi,
            accountDao = accountDao,
            tvShowDao = tvShowDao,
            dispatcher = TestCoroutineContextProvider(),
            connectivity = connectivity
        )
    }

    @Test
    fun `test getAccount when sessionId is valid and network is up`() = runBlockingTest {
        val sessionId = "12345"
        val accountResponse = AccountResponse(
            id = 1,
            name = "carlos menjivar",
            username = "@carlos.menjivar",
            avatar = AvatarResponse(TmdbResponse("fake_path"))
        )

        //given network access
        `when`(connectivity.hasNetworkAccess())
            .thenReturn(true)

        `when`(accountApi.getAccount(sessionId))
            .thenReturn(accountResponse)


        //when account is fetched using a valid id
        val response = accountRepository.getAccount(sessionId)

        //then verify the response match with original values
        assertTrue(response is ResultWrapper.Success)
        assertEquals(1, (response as ResultWrapper.Success).data.id)
        assertEquals("carlos menjivar", response.data.name)
        assertEquals("@carlos.menjivar", response.data.username)
        assertEquals("fake_path", response.data.avatar)

        //then also, the account is saved/updated on cache
        assertNotNull(accountDao.findBySessionId(sessionId))
    }

    @Test
    fun `test getAccount when sessionId is valid and network is down`() = runBlockingTest {
        val sessionId = "12345"
        val accountEntity = AccountEntity(
            id = 1,
            sessionId = sessionId,
            name = "carlos menjivar",
            username = "@carlos.menjivar",
            avatar = "fake_path"
        )

        accountDao.insertAccount(accountEntity)

        //given disabled the network
        `when`(connectivity.hasNetworkAccess())
            .thenReturn(false)

        //when account is fetched using a valid id
        val response = accountRepository.getAccount(sessionId)

        //then verify response is correct and data match with original values
        assertTrue(response is ResultWrapper.Success)
        assertEquals(1, (response as ResultWrapper.Success).data.id)
        assertEquals("carlos menjivar", response.data.name)
        assertEquals("@carlos.menjivar", response.data.username)
        assertEquals("fake_path", response.data.avatar)
    }

    @Test
    fun `test saveAccountData`() = runBlockingTest {
        //given a valid session
        val sessionId = "12345"
        val accountResponse = AccountResponse(
            id = 1,
            name = "carlos menjivar",
            username = "@carlos.menjivar",
            avatar = AvatarResponse(TmdbResponse("fake_path"))
        )

        `when`(accountApi.getAccount(sessionId))
            .thenReturn(accountResponse)

        //when account is saved successfully
        val response = accountRepository.saveAccountData(sessionId)

        //then verify the response is correct and data match with original values
        assertTrue(response is ResultWrapper.Success)
        assertEquals(1, (response as ResultWrapper.Success).data.id)
        assertEquals("carlos menjivar", response.data.name)
        assertEquals("@carlos.menjivar", response.data.username)
        assertEquals("fake_path", response.data.avatar)
    }

    @Test
    fun `test markTvShowAsFavorite`() = runBlockingTest {
        val sessionId = "12345"
        val accountId = 1
        val idTvShow = 1

        //when account is valid and tvShow is marked as favorite
        `when`(accountApi.getAccount(sessionId))
            .thenReturn(
                AccountResponse(
                    id = accountId,
                    name = "carlos menjivar",
                    username = "@carlos.menjivar",
                    avatar = AvatarResponse(TmdbResponse("fake_path"))
                )
            )
        `when`(accountApi.markTvShowAsFavorite(anyInt(), anyString(), any()))
            .thenReturn(GenericResponse(1, "ok"))

        tvShowDao.insertShow(
            listOf(
                TvShowEntity(
                    id = idTvShow,
                    name = "name",
                    originalName = "original",
                    posterPath = "",
                    voteAverage = 0f,
                    popularity = 0f,
                    overview = ""
                )
            )
        )

        val response = accountRepository.markTvShowAsFavorite(sessionId, 1, true)

        //then verify the response is correct and show is favorite
        assertTrue(response is ResultWrapper.Success)
        assertEquals(idTvShow, (response as ResultWrapper.Success).data.id)
        assertTrue(response.data.favorite)
    }

    @Test
    fun `test getFavoriteTvShow when network is up`() = runBlockingTest {
        val sessionId = "12345"
        val accountId = 1
        val idTvShow = 1
        val resultTvShowResponse = ResultTvShowResponse(
            page = 1,
            totalPages = 1,
            totalResults = 1,
            results = listOf(
                TvShowResponse(
                    id = idTvShow,
                    name = "sponge bob",
                    originalName = "sponge bob",
                    posterPath = "fake_path",
                    voteAverage = 1f,
                    popularity = 1f,
                    overview = "",
                    seasons = listOf()
                )
            )
        )

        //given disabled the network and valid account
        `when`(connectivity.hasNetworkAccess())
            .thenReturn(true)

        `when`(accountApi.getAccount(sessionId))
            .thenReturn(
                AccountResponse(
                    id = accountId,
                    name = "carlos menjivar",
                    username = "@carlos.menjivar",
                    avatar = AvatarResponse(TmdbResponse("fake_path"))
                )
            )

        `when`(accountApi.getFavoriteTvShow(accountId, sessionId))
            .thenReturn(resultTvShowResponse)


        //given list of favorite shows saved on cache
        tvShowDao.insertShow(resultTvShowResponse.results.map { it.mapToEntity() })

        //when favorite shows is fetched
        val response = accountRepository.getFavoriteTvShow(sessionId)

        //then verify the response is correct
        assertTrue(response is ResultWrapper.Success)
        assertEquals(1, (response as ResultWrapper.Success).data.results.size)

        //then verify if the data has been saved on cache
        assertEquals(idTvShow, tvShowDao.findById(idTvShow).id)

    }

    @Test
    fun `test getFavoriteTvShow when network is down`() = runBlockingTest {
        //given network down
        `when`(connectivity.hasNetworkAccess())
            .thenReturn(false)

        //when getFavoriteTvShow is fetched
        val response = accountRepository.getFavoriteTvShow("")

        //then verify the fetched data
        assertTrue(response is ResultWrapper.Success)
        assertEquals(1, (response as ResultWrapper.Success).data.page)
        assertEquals(1, response.data.totalPages)
        assertEquals(0, response.data.totalResults)
    }
}