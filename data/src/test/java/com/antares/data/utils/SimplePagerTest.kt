package com.antares.data.utils

import org.junit.Assert.assertEquals
import org.junit.Test

class SimplePagerTest {

    @Test
    fun `test getTotalPages having one single page`() {
        //given the maximum number of rows on one page
        val simplePager = SimplePager(
            pageNumber = 1,
            pageSize = 20,
            totalRows = 20
        )

        //then verify the number of pages
        assertEquals(1, simplePager.getTotalPages())
    }

    @Test
    fun `test getTotalPages having two pages`() {
        //given more rows than the maximum supported by one page
        val simplePager = SimplePager(
            pageNumber = 1,
            pageSize = 20,
            totalRows = 21
        )

        //then verify the number of pages
        assertEquals(2, simplePager.getTotalPages())
    }

    @Test
    fun `test getOffset having one page`() {
        //given the maximum number of rows on one page
        val simplePager = SimplePager(
            pageNumber = 1,
            pageSize = 20,
            totalRows = 20
        )

        //then verify the offset used on SQL QUERY (limit x offset y)
        assertEquals(0, simplePager.getOffset())
    }

    @Test
    fun `test getOffset having two pages`() {
        //given two full pages and starting from page 2
        val simplePager = SimplePager(
            pageNumber = 2,
            pageSize = 20,
            totalRows = 40
        )

        //then verify the offset used on SQL QUERY (limit x offset y)
        assertEquals(20, simplePager.getOffset())
    }

    @Test
    fun `test getOffset having three pages`() {
        //given three full pages and starting from page 3
        val simplePager = SimplePager(
            pageNumber = 3,
            pageSize = 20,
            totalRows = 60
        )

        //then verify the offset used on SQL QUERY (limit x offset y)
        assertEquals(40, simplePager.getOffset())
    }
}