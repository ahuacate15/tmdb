package com.antares.tmdb.ui.account

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.antares.domain.interactor.account.GetFavoriteTvShowUseCase
import com.antares.domain.interactor.auth.DeleteSessionUseCase
import com.antares.domain.interactor.auth.GetAccountUseCase
import com.antares.domain.model.ResultTvShow
import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.auth.Account
import com.antares.domain.model.auth.DeleteSession
import com.antares.tmdb.getOrAwaitValue
import com.antares.tmdb.utils.Constant
import com.antares.tmdb.utils.GenericViewModel
import com.antares.tmdb.utils.SharedPreferencesManager
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.*
import org.junit.Assert.assertTrue
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class AccountViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    @Mock
    private lateinit var getAccountUseCase: GetAccountUseCase

    @Mock
    private lateinit var deleteSessionUseCase: DeleteSessionUseCase

    @Mock
    private lateinit var getFavoriteTvShowUseCase: GetFavoriteTvShowUseCase

    @Mock
    private lateinit var sharedPreferencesManager: SharedPreferencesManager

    private lateinit var accountViewModel: AccountViewModel

    @Before
    fun setup() = runBlockingTest {
        accountViewModel = AccountViewModel(
            getAccountUseCase = getAccountUseCase,
            deleteSessionUseCase = deleteSessionUseCase,
            getFavoriteTvShowUseCase = getFavoriteTvShowUseCase,
            sharedPreferencesManager = sharedPreferencesManager,
            ioDispatcher = testCoroutineDispatcher
        )
    }

    @After
    fun tearDown() {
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `test loadProfile when data is success`() = testCoroutineDispatcher.runBlockingTest {
        val sessionId = "12345"
        //given a success account
        `when`(sharedPreferencesManager.getString(Constant.PREF_SESSION_ID))
            .thenReturn(sessionId)
        `when`(getAccountUseCase.invoke(sessionId))
            .thenReturn(
                ResultWrapper.Success(
                    Account(
                        id = 1,
                        name = "carlos menjivar",
                        username = "carlos.menjivar",
                        avatar = "fake_path"
                    )
                )
            )

        testCoroutineDispatcher.pauseDispatcher()

        //when loadProfile is called
        accountViewModel.loadProfile()

        val stateData = accountViewModel.dataObserver.getOrAwaitValue()

        //then verify when token is creating
        assertTrue(stateData is GenericViewModel.State.Loading)

        testCoroutineDispatcher.resumeDispatcher()
        val successState = accountViewModel.dataObserver.getOrAwaitValue()

        //then verify when profile has been created
        assertTrue(successState is GenericViewModel.State.Success)
        assertEquals(1, (successState as GenericViewModel.State.Success).data.id)
        assertEquals("carlos menjivar", successState.data.name)
        assertEquals("carlos.menjivar", successState.data.username)
        assertEquals("fake_path", successState.data.avatar)
    }

    @Test
    fun `test loadProfile when generic error is handled`() =
        testCoroutineDispatcher.runBlockingTest {
            val sessionId = "12345"

            //given a generic error
            `when`(sharedPreferencesManager.getString(Constant.PREF_SESSION_ID))
                .thenReturn(sessionId)
            `when`(getAccountUseCase.invoke(sessionId))
                .thenReturn(ResultWrapper.GenericError(code = 0, message = "internal error"))

            testCoroutineDispatcher.pauseDispatcher()

            //when token is created
            accountViewModel.loadProfile()
            val stateData = accountViewModel.dataObserver.getOrAwaitValue()

            //then verify when token is creating
            assertTrue(stateData is GenericViewModel.State.Loading)

            testCoroutineDispatcher.resumeDispatcher()
            val errorState = accountViewModel.dataObserver.getOrAwaitValue()

            //then verify when error occurs
            assertTrue(errorState is GenericViewModel.State.Error)
            Assert.assertEquals(0, (errorState as GenericViewModel.State.Error).errorCode)
            Assert.assertEquals("internal error", errorState.message)
        }

    @Test
    fun `test loadProfile when network is handled`() = testCoroutineDispatcher.runBlockingTest {
        val sessionId = "12345"

        //given a generic error
        `when`(sharedPreferencesManager.getString(Constant.PREF_SESSION_ID))
            .thenReturn(sessionId)
        `when`(getAccountUseCase.invoke(sessionId))
            .thenReturn(ResultWrapper.NetworkError)


        testCoroutineDispatcher.pauseDispatcher()

        //when token is created
        accountViewModel.loadProfile()
        val stateData = accountViewModel.dataObserver.getOrAwaitValue()

        //then verify when token is creating
        assertTrue(stateData is GenericViewModel.State.Loading)

        testCoroutineDispatcher.resumeDispatcher()
        val errorState = accountViewModel.dataObserver.getOrAwaitValue()

        //then verify when error occurs
        assertTrue(errorState is GenericViewModel.State.Error)
        Assert.assertEquals(
            Constant.ERROR_GENERIC,
            (errorState as GenericViewModel.State.Error).errorCode
        )
        Assert.assertNull(errorState.message)
    }

    @Test
    fun `test getFavoriteTvShow when data is success`() = testCoroutineDispatcher.runBlockingTest {
        val sessionId = "12345"
        //given a success account
        `when`(sharedPreferencesManager.getString(Constant.PREF_SESSION_ID))
            .thenReturn(sessionId)
        `when`(getFavoriteTvShowUseCase.invoke(sessionId))
            .thenReturn(
                ResultWrapper.Success(
                    ResultTvShow(
                        page = 1,
                        totalPages = 1,
                        totalResults = 0,
                        results = listOf()
                    )
                )
            )

        testCoroutineDispatcher.pauseDispatcher()

        //when getFavoriteTvShow is called
        accountViewModel.getFavoriteTvShow()

        val stateData = accountViewModel.observerFavoriteTvShow.getOrAwaitValue()

        //then verify when list of favorite show is fetching
        assertTrue(stateData is GenericViewModel.State.Loading)

        testCoroutineDispatcher.resumeDispatcher()
        val successState = accountViewModel.observerFavoriteTvShow.getOrAwaitValue()

        //then verify when list has been fetched
        assertTrue(successState is GenericViewModel.State.Success)
        assertEquals(1, (successState as GenericViewModel.State.Success).data.page)
        assertEquals(1, successState.data.totalPages)
        assertEquals(0, successState.data.totalResults)
        assertTrue(successState.data.results.isEmpty())
    }

    @Test
    fun `test getFavoriteTvShow when generic error is handled`() =
        testCoroutineDispatcher.runBlockingTest {
            val sessionId = "12345"

            //given a generic error
            `when`(sharedPreferencesManager.getString(Constant.PREF_SESSION_ID))
                .thenReturn(sessionId)
            `when`(getFavoriteTvShowUseCase.invoke(sessionId))
                .thenReturn(ResultWrapper.GenericError(code = 0, message = "internal error"))

            testCoroutineDispatcher.pauseDispatcher()

            //when getFavoriteTvShow is called
            accountViewModel.getFavoriteTvShow()
            val stateData = accountViewModel.observerFavoriteTvShow.getOrAwaitValue()

            //then verify when list of favorite show is fetching
            assertTrue(stateData is GenericViewModel.State.Loading)

            testCoroutineDispatcher.resumeDispatcher()
            val errorState = accountViewModel.observerFavoriteTvShow.getOrAwaitValue()

            //then verify when error occurs
            assertTrue(errorState is GenericViewModel.State.Error)
            Assert.assertEquals(0, (errorState as GenericViewModel.State.Error).errorCode)
            Assert.assertEquals("internal error", errorState.message)
        }

    @Test
    fun `test getFavoriteTvShow when network is handled`() =
        testCoroutineDispatcher.runBlockingTest {
            val sessionId = "12345"

            //given a generic error
            `when`(sharedPreferencesManager.getString(Constant.PREF_SESSION_ID))
                .thenReturn(sessionId)
            `when`(getFavoriteTvShowUseCase.invoke(sessionId))
                .thenReturn(ResultWrapper.NetworkError)


            testCoroutineDispatcher.pauseDispatcher()

            //when getFavoriteTvShow is called
            accountViewModel.getFavoriteTvShow()
            val stateData = accountViewModel.observerFavoriteTvShow.getOrAwaitValue()

            //then verify when list of favorite show is fetching
            assertTrue(stateData is GenericViewModel.State.Loading)

            testCoroutineDispatcher.resumeDispatcher()
            val errorState = accountViewModel.observerFavoriteTvShow.getOrAwaitValue()

            //then verify when error occurs
            assertTrue(errorState is GenericViewModel.State.Error)
            Assert.assertEquals(
                Constant.ERROR_GENERIC,
                (errorState as GenericViewModel.State.Error).errorCode
            )
            Assert.assertNull(errorState.message)
        }

    @Test
    fun `test deleteSession when data is success`() = testCoroutineDispatcher.runBlockingTest {
        val sessionId = "12345"
        //given a success account
        `when`(sharedPreferencesManager.getString(Constant.PREF_SESSION_ID))
            .thenReturn(sessionId)
        `when`(deleteSessionUseCase.invoke(sessionId))
            .thenReturn(
                ResultWrapper.Success(
                    DeleteSession(true)
                )
            )

        testCoroutineDispatcher.pauseDispatcher()

        //when deleteSession is called
        accountViewModel.deleteSession()

        val stateData = accountViewModel.dataObserverDeleteSession.getOrAwaitValue()

        //then verify when session is deleting
        assertTrue(stateData is GenericViewModel.State.Loading)

        testCoroutineDispatcher.resumeDispatcher()
        val successState = accountViewModel.dataObserverDeleteSession.getOrAwaitValue()

        //then verify when session has been deleted
        assertTrue(successState is GenericViewModel.State.Success)
        assertTrue((successState as GenericViewModel.State.Success).data.success)
    }

    @Test
    fun `test deleteSession when generic error is handled`() =
        testCoroutineDispatcher.runBlockingTest {
            val sessionId = "12345"

            //given a generic error
            `when`(sharedPreferencesManager.getString(Constant.PREF_SESSION_ID))
                .thenReturn(sessionId)
            `when`(deleteSessionUseCase.invoke(sessionId))
                .thenReturn(ResultWrapper.GenericError(code = 0, message = "internal error"))

            testCoroutineDispatcher.pauseDispatcher()

            //when deleteSession is called
            accountViewModel.deleteSession()
            val stateData = accountViewModel.dataObserverDeleteSession.getOrAwaitValue()

            //then verify when session is deleting
            assertTrue(stateData is GenericViewModel.State.Loading)

            testCoroutineDispatcher.resumeDispatcher()
            val errorState = accountViewModel.dataObserverDeleteSession.getOrAwaitValue()

            //then verify when error occurs
            assertTrue(errorState is GenericViewModel.State.Error)
            Assert.assertEquals(0, (errorState as GenericViewModel.State.Error).errorCode)
            Assert.assertEquals("internal error", errorState.message)
        }

    @Test
    fun `test deleteSession when network is handled`() =
        testCoroutineDispatcher.runBlockingTest {
            val sessionId = "12345"

            //given a generic error
            `when`(sharedPreferencesManager.getString(Constant.PREF_SESSION_ID))
                .thenReturn(sessionId)
            `when`(deleteSessionUseCase.invoke(sessionId))
                .thenReturn(ResultWrapper.NetworkError)


            testCoroutineDispatcher.pauseDispatcher()

            //when deleteSession is called
            accountViewModel.deleteSession()
            val stateData = accountViewModel.dataObserverDeleteSession.getOrAwaitValue()

            //then verify when session is deleting
            assertTrue(stateData is GenericViewModel.State.Loading)

            testCoroutineDispatcher.resumeDispatcher()
            val errorState = accountViewModel.dataObserverDeleteSession.getOrAwaitValue()

            //then verify when error occurs
            assertTrue(errorState is GenericViewModel.State.Error)
            Assert.assertEquals(
                Constant.ERROR_GENERIC,
                (errorState as GenericViewModel.State.Error).errorCode
            )
            Assert.assertNull(errorState.message)
        }
}