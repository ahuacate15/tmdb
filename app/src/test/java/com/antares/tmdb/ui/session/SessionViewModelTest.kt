package com.antares.tmdb.ui.session

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.antares.domain.interactor.account.SaveAccountDataUseCase
import com.antares.domain.interactor.auth.CreateNewSessionUseCase
import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.auth.GeneratedSession
import com.antares.tmdb.getOrAwaitValue
import com.antares.tmdb.utils.Constant
import com.antares.tmdb.utils.GenericViewModel
import com.antares.tmdb.utils.SharedPreferencesManager
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.*
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class SessionViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    @Mock
    private lateinit var createNewSessionUseCase: CreateNewSessionUseCase

    @Mock
    private lateinit var saveAccountDataUseCase: SaveAccountDataUseCase

    @Mock
    private lateinit var sharedPreferencesManager: SharedPreferencesManager

    private lateinit var sessionViewModel: SessionViewModel

    @Before
    fun setup() {
        sessionViewModel = SessionViewModel(
            createNewSessionUseCase = createNewSessionUseCase,
            saveAccountDataUseCase = saveAccountDataUseCase,
            sharedPreferencesManager = sharedPreferencesManager,
            ioDispatcher = testCoroutineDispatcher
        )
    }

    @After
    fun tearDown() {
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `test createNewSession when data is success`() = testCoroutineDispatcher.runBlockingTest {
        val requestToken = "12345"
        val sessionId = "00110011"

        //given a success response
        `when`(createNewSessionUseCase.invoke(requestToken))
            .thenReturn(
                ResultWrapper.Success(
                    GeneratedSession(
                        success = true,
                        sessionId = sessionId
                    )
                )
            )
        `when`(saveAccountDataUseCase.invoke(sessionId))
            .then { it.mock }

        testCoroutineDispatcher.pauseDispatcher()

        //when session in created
        sessionViewModel.createNewSession(requestToken)
        val stateData = sessionViewModel.dataObserver.getOrAwaitValue()

        //then verify while session is creating
        Assert.assertTrue(stateData is GenericViewModel.State.Loading)

        testCoroutineDispatcher.resumeDispatcher()
        val successState = sessionViewModel.dataObserver.getOrAwaitValue()

        //then verify when session has been created
        Assert.assertTrue(successState is GenericViewModel.State.Success)
        Assert.assertTrue((successState as GenericViewModel.State.Success).data as Boolean)
    }

    @Test
    fun `test createNewSession when generic error is handled`() =
        testCoroutineDispatcher.runBlockingTest {
            val requestToken = "12345"

            //given a generic error
            `when`(createNewSessionUseCase.invoke(requestToken))
                .thenReturn(
                    ResultWrapper.GenericError(code = 0, message = "internal error")
                )

            testCoroutineDispatcher.pauseDispatcher()

            //when session in created
            sessionViewModel.createNewSession(requestToken)
            val stateData = sessionViewModel.dataObserver.getOrAwaitValue()

            //then verify while session is creating
            Assert.assertTrue(stateData is GenericViewModel.State.Loading)

            testCoroutineDispatcher.resumeDispatcher()
            val errorState = sessionViewModel.dataObserver.getOrAwaitValue()

            //then verify when error occurs
            Assert.assertTrue(errorState is GenericViewModel.State.Error)
            Assert.assertEquals(0, (errorState as GenericViewModel.State.Error).errorCode)
            Assert.assertEquals("internal error", errorState.message)
        }

    @Test
    fun `test createNewSession when network is handled`() =
        testCoroutineDispatcher.runBlockingTest {
            val requestToken = "12345"

            //given a network error
            `when`(createNewSessionUseCase.invoke(requestToken))
                .thenReturn(ResultWrapper.NetworkError)

            testCoroutineDispatcher.pauseDispatcher()

            //when session in created
            sessionViewModel.createNewSession(requestToken)
            val stateData = sessionViewModel.dataObserver.getOrAwaitValue()

            //then verify while session is creating
            Assert.assertTrue(stateData is GenericViewModel.State.Loading)

            testCoroutineDispatcher.resumeDispatcher()
            val errorState = sessionViewModel.dataObserver.getOrAwaitValue()

            //then verify when error occurs
            Assert.assertTrue(errorState is GenericViewModel.State.Error)
            Assert.assertEquals(
                Constant.ERROR_GENERIC,
                (errorState as GenericViewModel.State.Error).errorCode
            )
            Assert.assertNull(errorState.message)
        }
}