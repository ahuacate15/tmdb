package com.antares.tmdb.ui.tvshowlist

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.antares.domain.interactor.tvshow.FindByPopularTvShowUseCase
import com.antares.domain.interactor.tvshow.FindOnAiringTodayShowUseCase
import com.antares.domain.interactor.tvshow.FindOnTheAirShowUseCase
import com.antares.domain.interactor.tvshow.FindTopTvShowUseCase
import com.antares.domain.model.ResultTvShow
import com.antares.domain.model.ResultWrapper
import com.antares.tmdb.getOrAwaitValue
import com.antares.tmdb.utils.GenericViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.*
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class TvShowListViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    @Mock
    private lateinit var findTopTvShowUseCase: FindTopTvShowUseCase

    @Mock
    private lateinit var findByPopularTvShowUseCase: FindByPopularTvShowUseCase

    @Mock
    private lateinit var findOnTheAirShowUseCase: FindOnTheAirShowUseCase

    @Mock
    private lateinit var findOnAiringTodayShowUseCase: FindOnAiringTodayShowUseCase

    private lateinit var tvShowListViewModel: TvShowListViewModel

    @Before
    fun setup() {
        tvShowListViewModel = TvShowListViewModel(
            findTopTvShowUseCase = findTopTvShowUseCase,
            findByPopularTvShowUseCase = findByPopularTvShowUseCase,
            findOnTheAirShowUseCase = findOnTheAirShowUseCase,
            findOnAiringTodayShowUseCase = findOnAiringTodayShowUseCase,
            ioDispatcher = testCoroutineDispatcher
        )
    }

    @After
    fun tearDown() {
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `test findTopShow when data is success`() = testCoroutineDispatcher.runBlockingTest {
        val page = 1

        //given a success data
        `when`(findTopTvShowUseCase.invoke(page))
            .thenReturn(
                ResultWrapper.Success(
                    ResultTvShow(
                        page = 1,
                        totalPages = 1,
                        totalResults = 1,
                        results = listOf()
                    )
                )
            )

        testCoroutineDispatcher.pauseDispatcher()

        //when top shows are fetched
        tvShowListViewModel.findTopShow(page)
        val stateData = tvShowListViewModel.dataObserver.getOrAwaitValue()

        //then verify when list is fetching
        Assert.assertTrue(stateData is GenericViewModel.State.Loading)

        testCoroutineDispatcher.resumeDispatcher()
        val successState = tvShowListViewModel.dataObserver.getOrAwaitValue()

        //then verify when data has been fetched
        Assert.assertTrue(successState is GenericViewModel.State.Success)
    }

    @Test
    fun `test findPopularShow when data is success`() = testCoroutineDispatcher.runBlockingTest {
        val page = 1

        //given a success data
        `when`(findByPopularTvShowUseCase.invoke(page))
            .thenReturn(
                ResultWrapper.Success(
                    ResultTvShow(
                        page = 1,
                        totalPages = 1,
                        totalResults = 1,
                        results = listOf()
                    )
                )
            )

        testCoroutineDispatcher.pauseDispatcher()

        //when top shows are fetched
        tvShowListViewModel.findPopularShow(page)
        val stateData = tvShowListViewModel.dataObserver.getOrAwaitValue()

        //then verify when list is fetching
        Assert.assertTrue(stateData is GenericViewModel.State.Loading)

        testCoroutineDispatcher.resumeDispatcher()
        val successState = tvShowListViewModel.dataObserver.getOrAwaitValue()

        //then verify when data has been fetched
        Assert.assertTrue(successState is GenericViewModel.State.Success)
    }

    @Test
    fun `test findOnTheAirShow when data is success`() = testCoroutineDispatcher.runBlockingTest {
        val page = 1

        //given a success data
        `when`(findOnTheAirShowUseCase.invoke(page))
            .thenReturn(
                ResultWrapper.Success(
                    ResultTvShow(
                        page = 1,
                        totalPages = 1,
                        totalResults = 1,
                        results = listOf()
                    )
                )
            )

        testCoroutineDispatcher.pauseDispatcher()

        //when top shows are fetched
        tvShowListViewModel.findOnTheAirShow(page)
        val stateData = tvShowListViewModel.dataObserver.getOrAwaitValue()

        //then verify when list is fetching
        Assert.assertTrue(stateData is GenericViewModel.State.Loading)

        testCoroutineDispatcher.resumeDispatcher()
        val successState = tvShowListViewModel.dataObserver.getOrAwaitValue()

        //then verify when data has been fetched
        Assert.assertTrue(successState is GenericViewModel.State.Success)
    }

    @Test
    fun `test findOnAiringTodayShow when data is success`() =
        testCoroutineDispatcher.runBlockingTest {
            val page = 1

            //given a success data
            `when`(findOnAiringTodayShowUseCase.invoke(page))
                .thenReturn(
                    ResultWrapper.Success(
                        ResultTvShow(
                            page = 1,
                            totalPages = 1,
                            totalResults = 1,
                            results = listOf()
                        )
                    )
                )

            testCoroutineDispatcher.pauseDispatcher()

            //when top shows are fetched
            tvShowListViewModel.findOnAiringTodayShow(page)
            val stateData = tvShowListViewModel.dataObserver.getOrAwaitValue()

            //then verify when list is fetching
            Assert.assertTrue(stateData is GenericViewModel.State.Loading)

            testCoroutineDispatcher.resumeDispatcher()
            val successState = tvShowListViewModel.dataObserver.getOrAwaitValue()

            //then verify when data has been fetched
            Assert.assertTrue(successState is GenericViewModel.State.Success)
        }
}