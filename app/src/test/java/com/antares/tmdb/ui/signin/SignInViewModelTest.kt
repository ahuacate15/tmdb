package com.antares.tmdb.ui.signin

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.antares.tmdb.getOrAwaitValue
import com.antares.tmdb.utils.Constant
import com.antares.tmdb.utils.SharedPreferencesManager
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class SignInViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    @Mock
    private lateinit var sharedPreferencesManager: SharedPreferencesManager

    private lateinit var signInViewModel: SignInViewModel

    @Before
    fun setup() {
        signInViewModel = SignInViewModel(
            sharedPreferencesManager = sharedPreferencesManager,
            ioDispatcher = testCoroutineDispatcher
        )
    }

    @After
    fun tearDown() {
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `test verifySession when session exists`() = testCoroutineDispatcher.runBlockingTest {
        `when`(sharedPreferencesManager.getString(Constant.PREF_SESSION_ID))
            .thenReturn("1000101")

        signInViewModel.verifySession()
        val stateData = signInViewModel.dataSessionObserver.getOrAwaitValue()

        assertTrue(stateData is SignInViewModel.State.AlreadyLogin)
    }

    @Test
    fun `test verifySession when session doesn't exits`() =
        testCoroutineDispatcher.runBlockingTest {
            `when`(sharedPreferencesManager.getString(Constant.PREF_SESSION_ID))
                .thenReturn(null)

            signInViewModel.verifySession()
            val stateData = signInViewModel.dataSessionObserver.getOrAwaitValue()

            assertTrue(stateData is SignInViewModel.State.NotLoggedYet)
        }
}