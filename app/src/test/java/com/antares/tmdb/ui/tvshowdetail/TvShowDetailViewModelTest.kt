package com.antares.tmdb.ui.tvshowdetail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.antares.domain.interactor.account.MarkTvShowAsFavoriteUseCase
import com.antares.domain.interactor.tvshow.FindTvShowByIdUseCase
import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.TvShow
import com.antares.tmdb.getOrAwaitValue
import com.antares.tmdb.utils.Constant
import com.antares.tmdb.utils.GenericViewModel
import com.antares.tmdb.utils.SharedPreferencesManager
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.*
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class TvShowDetailViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    @Mock
    private lateinit var findTvShowByIdUseCase: FindTvShowByIdUseCase

    @Mock
    private lateinit var markTvShowAsFavoriteUseCase: MarkTvShowAsFavoriteUseCase

    @Mock
    private lateinit var sharedPreferencesManager: SharedPreferencesManager

    private lateinit var tvShowDetailViewModel: TvShowDetailViewModel

    @Before
    fun setup() {
        tvShowDetailViewModel = TvShowDetailViewModel(
            findTvShowByIdUseCase = findTvShowByIdUseCase,
            markTvShowAsFavoriteUseCase = markTvShowAsFavoriteUseCase,
            sharedPreferencesManager = sharedPreferencesManager,
            ioDispatcher = testCoroutineDispatcher
        )
    }

    @After
    fun tearDown() {
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `test findTvShowById is success`() = testCoroutineDispatcher.runBlockingTest {
        val idTvShow = 1

        //given a success response
        `when`(findTvShowByIdUseCase.invoke(idTvShow))
            .thenReturn(
                ResultWrapper.Success(
                    TvShow(
                        id = idTvShow,
                        name = "spongebob",
                        originalName = "spongebob",
                        posterPath = "fake_path",
                        voteAverage = 0f,
                        popularity = 0f,
                        overview = "overview",
                        favorite = false,
                        seasons = null
                    )
                )
            )
        testCoroutineDispatcher.pauseDispatcher()

        //when list of tvShow is fetched
        tvShowDetailViewModel.findTvShowById(idTvShow)
        val stateData = tvShowDetailViewModel.dataObserver.getOrAwaitValue()

        //then verify while detail is loading
        Assert.assertTrue(stateData is GenericViewModel.State.Loading)

        testCoroutineDispatcher.resumeDispatcher()
        val successState = tvShowDetailViewModel.dataObserver.getOrAwaitValue()

        //then verify the detail of tvShow
        Assert.assertTrue(successState is GenericViewModel.State.Success)
        Assert.assertEquals(idTvShow, (successState as GenericViewModel.State.Success).data.id)
    }

    @Test
    fun `test findTvShowById when generic error is handled`() = testCoroutineDispatcher.runBlockingTest {
        val idTvShow = 1

        //given a generic error
        `when`(findTvShowByIdUseCase.invoke(idTvShow))
            .thenReturn(
                ResultWrapper.GenericError(code = 0, message = "internal error")
            )

        testCoroutineDispatcher.pauseDispatcher()

        //when list of tvShow is fetched
        tvShowDetailViewModel.findTvShowById(idTvShow)
        val stateData = tvShowDetailViewModel.dataObserver.getOrAwaitValue()

        //then verify while detail is loading
        Assert.assertTrue(stateData is GenericViewModel.State.Loading)

        testCoroutineDispatcher.resumeDispatcher()
        val errorState = tvShowDetailViewModel.dataObserver.getOrAwaitValue()

        //then verify when error occurs
        Assert.assertTrue(errorState is GenericViewModel.State.Error)
        Assert.assertEquals(0, (errorState as GenericViewModel.State.Error).errorCode)
        Assert.assertEquals("internal error", errorState.message)
    }

    @Test
    fun `test createToken when network is handled`() = testCoroutineDispatcher.runBlockingTest {
        val idTvShow = 1

        //given a network error
        `when`(findTvShowByIdUseCase.invoke(idTvShow))
            .thenReturn(ResultWrapper.NetworkError)

        testCoroutineDispatcher.pauseDispatcher()

        //when token is created
        tvShowDetailViewModel.findTvShowById(idTvShow)
        val stateData = tvShowDetailViewModel.dataObserver.getOrAwaitValue()

        //then verify when token is creating
        Assert.assertTrue(stateData is GenericViewModel.State.Loading)

        testCoroutineDispatcher.resumeDispatcher()
        val errorState = tvShowDetailViewModel.dataObserver.getOrAwaitValue()

        //then verify when error occurs
        Assert.assertTrue(errorState is GenericViewModel.State.Error)
        Assert.assertEquals(
            Constant.ERROR_GENERIC,
            (errorState as GenericViewModel.State.Error).errorCode
        )
        Assert.assertNull(errorState.message)
    }

    @Test
    fun `test markAsFavorite is success`() = testCoroutineDispatcher.runBlockingTest {
        val idTvShow = 1
        val favorite = true
        val sessionId = "0011000"

        //given a success response
        `when`(sharedPreferencesManager.getString(Constant.PREF_SESSION_ID))
            .thenReturn(sessionId)

        `when`(markTvShowAsFavoriteUseCase.invoke(sessionId, idTvShow, favorite))
            .thenReturn(
                ResultWrapper.Success(
                    TvShow(
                        id = idTvShow,
                        name = "spongebob",
                        originalName = "spongebob",
                        posterPath = "fake_path",
                        voteAverage = 0f,
                        popularity = 0f,
                        overview = "overview",
                        favorite = favorite,
                        seasons = null
                    )
                )
            )
        testCoroutineDispatcher.pauseDispatcher()

        //when list of tvShow is fetched
        tvShowDetailViewModel.markAsFavorite(idTvShow, favorite)
        val stateData = tvShowDetailViewModel.dataObserverMarkAsFavorite.getOrAwaitValue()

        //then verify action is loading
        Assert.assertTrue(stateData is GenericViewModel.State.Loading)

        testCoroutineDispatcher.resumeDispatcher()
        val successState = tvShowDetailViewModel.dataObserverMarkAsFavorite.getOrAwaitValue()

        //then verify when show has been marked as favorite
        Assert.assertTrue(successState is GenericViewModel.State.Success)
        Assert.assertEquals(idTvShow, (successState as GenericViewModel.State.Success).data.id)
        Assert.assertEquals(favorite, successState.data.favorite)
    }

    @Test
    fun `test markAsFavorite when generic error is handled`() = testCoroutineDispatcher.runBlockingTest {
        val idTvShow = 1
        val favorite = true
        val sessionId = "0011000"

        //given a generic error
        `when`(sharedPreferencesManager.getString(Constant.PREF_SESSION_ID))
            .thenReturn(sessionId)

        `when`(markTvShowAsFavoriteUseCase.invoke(sessionId, idTvShow, favorite))
            .thenReturn(
                ResultWrapper.GenericError(code = 0, message = "internal error")
            )

        testCoroutineDispatcher.pauseDispatcher()

        //when list of tvShow is fetched
        tvShowDetailViewModel.markAsFavorite(idTvShow, favorite)
        val stateData = tvShowDetailViewModel.dataObserverMarkAsFavorite.getOrAwaitValue()

        //then verify action is loading
        Assert.assertTrue(stateData is GenericViewModel.State.Loading)

        testCoroutineDispatcher.resumeDispatcher()
        val errorState = tvShowDetailViewModel.dataObserverMarkAsFavorite.getOrAwaitValue()

        //then verify when error occurs
        Assert.assertTrue(errorState is GenericViewModel.State.Error)
        Assert.assertEquals(0, (errorState as GenericViewModel.State.Error).errorCode)
        Assert.assertEquals("internal error", errorState.message)
    }

    @Test
    fun `test markAsFavorite when network is handled`() = testCoroutineDispatcher.runBlockingTest {
        val idTvShow = 1
        val favorite = true
        val sessionId = "0011000"

        //given a network error
        `when`(sharedPreferencesManager.getString(Constant.PREF_SESSION_ID))
            .thenReturn(sessionId)

        `when`(markTvShowAsFavoriteUseCase.invoke(sessionId, idTvShow, favorite))
            .thenReturn(ResultWrapper.NetworkError)

        testCoroutineDispatcher.pauseDispatcher()

        //when token is created
        tvShowDetailViewModel.markAsFavorite(idTvShow, favorite)
        val stateData = tvShowDetailViewModel.dataObserverMarkAsFavorite.getOrAwaitValue()

        //then verify when token is creating
        Assert.assertTrue(stateData is GenericViewModel.State.Loading)

        testCoroutineDispatcher.resumeDispatcher()
        val errorState = tvShowDetailViewModel.dataObserverMarkAsFavorite.getOrAwaitValue()

        //then verify when error occurs
        Assert.assertTrue(errorState is GenericViewModel.State.Error)
        Assert.assertEquals(
            Constant.ERROR_GENERIC,
            (errorState as GenericViewModel.State.Error).errorCode
        )
        Assert.assertNull(errorState.message)
    }
}