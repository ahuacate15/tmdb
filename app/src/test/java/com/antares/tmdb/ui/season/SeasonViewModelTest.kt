package com.antares.tmdb.ui.season

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.antares.domain.interactor.season.FindSeasonByIdShowAndSeasonNumberUseCase
import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.Season
import com.antares.tmdb.getOrAwaitValue
import com.antares.tmdb.utils.Constant
import com.antares.tmdb.utils.GenericViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.*
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class SeasonViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    @Mock
    private lateinit var findSeasonByIdShowAndSeasonNumberUseCase: FindSeasonByIdShowAndSeasonNumberUseCase

    private lateinit var seasonViewModel: SeasonViewModel

    @Before
    fun setup() {
        seasonViewModel = SeasonViewModel(
            findSeasonByIdShowAndSeasonNumberUseCase = findSeasonByIdShowAndSeasonNumberUseCase,
            ioDispatcher = testCoroutineDispatcher
        )
    }

    @After
    fun tearDown() {
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `test findSeasonByIdShowAndSeasonNumber when data is success`() =
        testCoroutineDispatcher.runBlockingTest {
            val idTvShow = 1
            val seasonNumber = 1

            //given a success response
            Mockito.`when`(findSeasonByIdShowAndSeasonNumberUseCase.invoke(idTvShow, seasonNumber))
                .thenReturn(
                    ResultWrapper.Success(
                        Season(
                            id = 100,
                            name = "season 1",
                            overview = "overview",
                            posterPath = "fake_path",
                            seasonNumber = seasonNumber,
                            episodeCount = 1,
                            episodes = listOf()
                        )
                    )
                )

            testCoroutineDispatcher.pauseDispatcher()

            //when season is fetched
            seasonViewModel.findSeasonByIdShowAndSeasonNumber(idTvShow, seasonNumber)
            val stateData = seasonViewModel.dataObserver.getOrAwaitValue()

            //then verify when season is loading
            Assert.assertTrue(stateData is GenericViewModel.State.Loading)

            testCoroutineDispatcher.resumeDispatcher()
            val successState = seasonViewModel.dataObserver.getOrAwaitValue()

            //then verify when season has been fetched
            Assert.assertTrue(successState is GenericViewModel.State.Success)
            Assert.assertEquals(100, (successState as GenericViewModel.State.Success).data.id)
        }

    @Test
    fun `test findSeasonByIdShowAndSeasonNumber when generic error is handled`() =
        testCoroutineDispatcher.runBlockingTest {
            val idTvShow = 1
            val seasonNumber = 1

            //given a generic error
            Mockito.`when`(findSeasonByIdShowAndSeasonNumberUseCase.invoke(idTvShow, seasonNumber))
                .thenReturn(
                    ResultWrapper.GenericError(code = 0, message = "internal error")
                )

            testCoroutineDispatcher.pauseDispatcher()

            //when season is fetched
            seasonViewModel.findSeasonByIdShowAndSeasonNumber(idTvShow, seasonNumber)
            val stateData = seasonViewModel.dataObserver.getOrAwaitValue()

            //then verify when season is loading
            Assert.assertTrue(stateData is GenericViewModel.State.Loading)

            testCoroutineDispatcher.resumeDispatcher()
            val errorState = seasonViewModel.dataObserver.getOrAwaitValue()

            //then verify when season has been fetched
            Assert.assertTrue(errorState is GenericViewModel.State.Error)
            Assert.assertEquals(0, (errorState as GenericViewModel.State.Error).errorCode)
            Assert.assertEquals("internal error", errorState.message)
        }

    @Test
    fun `test createToken when network is handled`() = testCoroutineDispatcher.runBlockingTest {
        val idTvShow = 1
        val seasonNumber = 1

        //given a network error
        Mockito.`when`(findSeasonByIdShowAndSeasonNumberUseCase.invoke(idTvShow, seasonNumber))
            .thenReturn(ResultWrapper.NetworkError)

        testCoroutineDispatcher.pauseDispatcher()

        //when season is fetched
        seasonViewModel.findSeasonByIdShowAndSeasonNumber(idTvShow, seasonNumber)
        val stateData = seasonViewModel.dataObserver.getOrAwaitValue()

        //then verify when season is loading
        Assert.assertTrue(stateData is GenericViewModel.State.Loading)

        testCoroutineDispatcher.resumeDispatcher()
        val errorState = seasonViewModel.dataObserver.getOrAwaitValue()

        //then verify when season has been fetched
        Assert.assertTrue(errorState is GenericViewModel.State.Error)
        Assert.assertEquals(
            Constant.ERROR_GENERIC,
            (errorState as GenericViewModel.State.Error).errorCode
        )
        Assert.assertNull(errorState.message)
    }

}