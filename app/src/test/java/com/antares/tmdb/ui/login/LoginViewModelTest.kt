package com.antares.tmdb.ui.login

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.antares.domain.interactor.auth.CreateTemporaryTokenUseCase
import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.auth.TmpToken
import com.antares.tmdb.getOrAwaitValue
import com.antares.tmdb.utils.Constant
import com.antares.tmdb.utils.GenericViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class LoginViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    @Mock
    private lateinit var createTemporaryTokenUseCase: CreateTemporaryTokenUseCase

    private lateinit var loginViewModel: LoginViewModel

    @Before
    fun setup() {
        loginViewModel = LoginViewModel(
            createTemporaryTokenUseCase = createTemporaryTokenUseCase,
            ioDispatcher = testCoroutineDispatcher
        )
    }

    @After
    fun tearDown() {
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `test createToken when data is success`() = testCoroutineDispatcher.runBlockingTest {
        //given a success response
        `when`(createTemporaryTokenUseCase.invoke())
            .thenReturn(
                ResultWrapper.Success(
                    TmpToken(
                        success = true,
                        requestToken = "12345"
                    )
                )
            )

        testCoroutineDispatcher.pauseDispatcher()

        //when token is created
        loginViewModel.createToken()
        val stateData = loginViewModel.dataObserver.getOrAwaitValue()

        //then verify when token is creating
        assertTrue(stateData is GenericViewModel.State.Loading)

        testCoroutineDispatcher.resumeDispatcher()
        val successState = loginViewModel.dataObserver.getOrAwaitValue()

        //then verify when token has been created
        assertTrue(successState is GenericViewModel.State.Success)
        assertTrue((successState as GenericViewModel.State.Success).data.success)
        assertEquals("12345", successState.data.requestToken)
    }

    @Test
    fun `test createToken when generic error is handled`() = testCoroutineDispatcher.runBlockingTest {
        //given a generic error
        `when`(createTemporaryTokenUseCase.invoke())
            .thenReturn(
                ResultWrapper.GenericError(code = 0, message = "internal error")
            )

        testCoroutineDispatcher.pauseDispatcher()

        //when token is created
        loginViewModel.createToken()
        val stateData = loginViewModel.dataObserver.getOrAwaitValue()

        //then verify when token is creating
        assertTrue(stateData is GenericViewModel.State.Loading)

        testCoroutineDispatcher.resumeDispatcher()
        val errorState = loginViewModel.dataObserver.getOrAwaitValue()

        //then verify when error occurs
        assertTrue(errorState is GenericViewModel.State.Error)
        assertEquals(0, (errorState as GenericViewModel.State.Error).errorCode)
        assertEquals("internal error", errorState.message)
    }

    @Test
    fun `test createToken when network is handled`() = testCoroutineDispatcher.runBlockingTest {
        //given a network error
        `when`(createTemporaryTokenUseCase.invoke())
            .thenReturn(ResultWrapper.NetworkError)

        testCoroutineDispatcher.pauseDispatcher()

        //when token is created
        loginViewModel.createToken()
        val stateData = loginViewModel.dataObserver.getOrAwaitValue()

        //then verify when token is creating
        assertTrue(stateData is GenericViewModel.State.Loading)

        testCoroutineDispatcher.resumeDispatcher()
        val errorState = loginViewModel.dataObserver.getOrAwaitValue()

        //then verify when error occurs
        assertTrue(errorState is GenericViewModel.State.Error)
        assertEquals(Constant.ERROR_GENERIC, (errorState as GenericViewModel.State.Error).errorCode)
        assertNull(errorState.message)
    }
}
