package com.antares.tmdb.di

import android.content.Context
import com.antares.domain.interactor.account.GetFavoriteTvShowUseCase
import com.antares.domain.interactor.account.MarkTvShowAsFavoriteUseCase
import com.antares.domain.interactor.account.SaveAccountDataUseCase
import com.antares.domain.interactor.auth.CreateNewSessionUseCase
import com.antares.domain.interactor.auth.CreateTemporaryTokenUseCase
import com.antares.domain.interactor.auth.DeleteSessionUseCase
import com.antares.domain.interactor.auth.GetAccountUseCase
import com.antares.domain.interactor.season.FindSeasonByIdShowAndSeasonNumberUseCase
import com.antares.domain.interactor.tvshow.*
import com.antares.tmdb.ui.account.AccountViewModel
import com.antares.tmdb.ui.login.LoginViewModel
import com.antares.tmdb.ui.season.SeasonViewModel
import com.antares.tmdb.ui.session.SessionViewModel
import com.antares.tmdb.ui.signin.SignInViewModel
import com.antares.tmdb.ui.tvshowdetail.TvShowDetailViewModel
import com.antares.tmdb.ui.tvshowlist.TvShowListViewModel
import com.antares.tmdb.utils.SharedPreferencesManager
import com.antares.tmdb.utils.SharedPreferencesManagerImp
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ActivityContext
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object PresentationModule {

    @Singleton
    @Provides
    fun providesIoDispatcher(): CoroutineDispatcher = Dispatchers.IO

    @Singleton
    @Provides
    fun provideSharedPreferencesManager(
        @ApplicationContext context: Context
    ): SharedPreferencesManager {
        return SharedPreferencesManagerImp(context)
    }

    @Singleton
    @Provides
    fun provideSignInViewModel(
        sharedPreferencesManager: SharedPreferencesManager,
        coroutineDispatcher: CoroutineDispatcher
    ): SignInViewModel {
        return SignInViewModel(sharedPreferencesManager, coroutineDispatcher)
    }

    @ActivityContext
    @Provides
    fun provideLoginViewModel(
        createTemporaryTokenUseCase: CreateTemporaryTokenUseCase,
        coroutineDispatcher: CoroutineDispatcher
    ): LoginViewModel {
        return LoginViewModel(createTemporaryTokenUseCase, coroutineDispatcher)
    }

    @ActivityContext
    @Provides
    fun provideSessionViewModel(
        createNewSessionUseCase: CreateNewSessionUseCase,
        saveAccountDataUseCase: SaveAccountDataUseCase,
        sharedPreferencesManager: SharedPreferencesManager,
        coroutineDispatcher: CoroutineDispatcher
    ): SessionViewModel {
        return SessionViewModel(
            createNewSessionUseCase,
            saveAccountDataUseCase,
            sharedPreferencesManager,
            coroutineDispatcher
        )
    }

    @ActivityContext
    @Provides
    fun provideTvShowListViewModel(
        findTopTvShowUseCase: FindTopTvShowUseCase,
        findByPopularTvShowUseCase: FindByPopularTvShowUseCase,
        findOnTheAirShowUseCase: FindOnTheAirShowUseCase,
        findOnAiringTodayShowUseCase: FindOnAiringTodayShowUseCase,
        coroutineDispatcher: CoroutineDispatcher
    ): TvShowListViewModel {
        return TvShowListViewModel(
            findTopTvShowUseCase,
            findByPopularTvShowUseCase,
            findOnTheAirShowUseCase,
            findOnAiringTodayShowUseCase,
            coroutineDispatcher
        )
    }

    @ActivityContext
    @Provides
    fun provideTvShowDetailViewModel(
        findTvShowByIdUseCase: FindTvShowByIdUseCase,
        markTvShowAsFavoriteUseCase: MarkTvShowAsFavoriteUseCase,
        sharedPreferencesManager: SharedPreferencesManager,
        coroutineDispatcher: CoroutineDispatcher
    ): TvShowDetailViewModel {
        return TvShowDetailViewModel(
            findTvShowByIdUseCase,
            markTvShowAsFavoriteUseCase,
            sharedPreferencesManager,
            coroutineDispatcher
        )
    }

    @ActivityContext
    @Provides
    fun provideSeasonViewModel(
        findSeasonByIdShowAndSeasonNumberUseCase: FindSeasonByIdShowAndSeasonNumberUseCase,
        coroutineDispatcher: CoroutineDispatcher
    ): SeasonViewModel {
        return SeasonViewModel(findSeasonByIdShowAndSeasonNumberUseCase, coroutineDispatcher)
    }

    @ActivityContext
    @Provides
    fun provideAccountViewModel(
        getAccountUseCase: GetAccountUseCase,
        getFavoriteTvShowUseCase: GetFavoriteTvShowUseCase,
        sharedPreferencesManager: SharedPreferencesManager,
        deleteSessionUseCase: DeleteSessionUseCase,
        coroutineDispatcher: CoroutineDispatcher
    ): AccountViewModel {
        return AccountViewModel(
            getAccountUseCase,
            deleteSessionUseCase,
            getFavoriteTvShowUseCase,
            sharedPreferencesManager,
            coroutineDispatcher
        )
    }
}