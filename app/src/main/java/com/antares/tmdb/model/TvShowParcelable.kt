package com.antares.tmdb.model

import android.os.Parcelable
import com.antares.data.utils.DomainEntity
import com.antares.domain.model.TvShow
import kotlinx.android.parcel.Parcelize

@Parcelize
class TvShowParcelable(
    val id: Int,
    val name: String,
    val originalName: String,
    val posterPath: String? = "",
    val voteAverage: Float,
    val overview: String,
    val popularity: Float
) : Parcelable, DomainEntity<TvShow> {

    override fun mapToDomain(): TvShow =
        TvShow(
            id = id,
            name = name,
            originalName = originalName,
            posterPath = posterPath,
            voteAverage = voteAverage,
            overview = overview,
            popularity = popularity,
            seasons = listOf()
        )

}