package com.antares.tmdb.extension

import com.antares.domain.model.TvShow
import com.antares.tmdb.model.TvShowParcelable

fun TvShow.toParcelable(): TvShowParcelable = TvShowParcelable(
    id = id,
    name = name,
    originalName = originalName,
    posterPath = posterPath,
    voteAverage = voteAverage,
    overview = overview,
    popularity = popularity
)