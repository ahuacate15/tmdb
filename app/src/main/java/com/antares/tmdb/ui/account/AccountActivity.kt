package com.antares.tmdb.ui.account

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.antares.tmdb.R
import com.antares.tmdb.databinding.ActivityAccountBinding
import com.antares.tmdb.extension.toParcelable
import com.antares.tmdb.ui.signin.SignInActivity
import com.antares.tmdb.ui.tvshowdetail.TvShowDetailActivity
import com.antares.tmdb.ui.tvshowlist.TvShowListAdapter
import com.antares.tmdb.utils.Constant
import com.antares.tmdb.utils.GenericViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AccountActivity : AppCompatActivity() {

    private val viewModel: AccountViewModel by viewModels()
    private lateinit var binding: ActivityAccountBinding
    private lateinit var tvShowListAdapter: TvShowListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_account)

        //add back button
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.btnLogOut.setOnClickListener { confirmLogout() }
        setFavoriteTvShowAdapter()
        loadProfileData()
        getFavoriteTvShow()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        onBackPressed()
        return true
    }

    private fun setFavoriteTvShowAdapter() {
        tvShowListAdapter = TvShowListAdapter(
            isHorizontalList = true,
            cardWidth = getCardWidth(),
            onClickListener = {
                val intent = Intent(this, TvShowDetailActivity::class.java).apply {
                    putExtra(Constant.PARAM_PARCELABLE_TV_SHOW, it.toParcelable())
                }
                startActivity(intent)
            }
        ).apply { setData(listOf(), false) }

        binding.recyclerFavorites.apply {
            layoutManager =
                LinearLayoutManager(this@AccountActivity, LinearLayoutManager.HORIZONTAL, false)
            adapter = tvShowListAdapter
        }
    }

    private fun getFavoriteTvShow() {
        viewModel.getFavoriteTvShow()
        viewModel.observerFavoriteTvShow.observe(this, { action ->
            when (action) {
                is GenericViewModel.State.Loading -> {
                    Log.d(TAG, "loading favorite...")
                }
                is GenericViewModel.State.Success -> {
                    Log.d(TAG, "favorite list loading: ${action.data}")
                    tvShowListAdapter.setData(
                        data = action.data.results,
                        appendData = false
                    )
                }
                is GenericViewModel.State.Error -> {
                    Log.d(TAG, "error to load favorite show: ${action.message}")
                    Toast.makeText(
                        this,
                        action.message ?: getString(R.string.err_load_favorite_tv_show),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })
    }

    private fun loadProfileData() {
        viewModel.loadProfile()
        viewModel.dataObserver.observe(this, { action ->
            when (action) {
                is GenericViewModel.State.Loading -> {
                    Log.d(TAG, "loading...")
                    binding.progressLoadingProfile.visibility = View.VISIBLE
                }
                is GenericViewModel.State.Success -> {
                    Log.d(TAG, "success: ${action.data}")
                    binding.profile = action.data
                    binding.progressLoadingProfile.visibility = View.GONE
                }
                is GenericViewModel.State.Error -> {
                    binding.progressLoadingProfile.visibility = View.GONE

                    //verify the type of error
                    if (action.errorCode == Constant.ERROR_SESSION_NOT_EXIT) {
                        sendToSignIn()
                    } else {
                        Log.d(TAG, "error: ${action.message}")
                        Toast.makeText(
                            this,
                            action.message ?: getString(R.string.err_load_profile),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        })
    }

    private fun confirmLogout() {
        val listener = DialogInterface.OnClickListener { dialog, which ->
            when (which) {
                DialogInterface.BUTTON_POSITIVE -> {
                    dialog.dismiss()
                }
                DialogInterface.BUTTON_NEGATIVE -> {
                    logOut()
                }
            }
        }

        val builder = AlertDialog.Builder(this)
        val dialog = builder.setMessage(getString(R.string.text_confirm_logout))
            .setPositiveButton(getString(R.string.text_stay), listener)
            .setNegativeButton(getString(R.string.text_leave), listener)
            .create()
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
            .setTextColor(ContextCompat.getColor(this, R.color.pink_400))
    }

    private fun logOut() {
        viewModel.deleteSession()
        viewModel.dataObserverDeleteSession.observe(this, { action ->
            when (action) {
                is GenericViewModel.State.Loading -> {
                    Log.d(TAG, "deleting session...")
                    binding.btnLogOut.text = getString(R.string.btn_logout_loading)
                    binding.btnLogOut.isEnabled = false
                }
                is GenericViewModel.State.Success -> {
                    Log.d(TAG, "session deleted")
                    binding.btnLogOut.text = getString(R.string.btn_logout)
                    binding.btnLogOut.isEnabled = true
                    sendToSignIn()
                }
                is GenericViewModel.State.Error -> {
                    binding.btnLogOut.isEnabled = true
                    Toast.makeText(
                        this,
                        action.message ?: getString(R.string.err_logout),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })
    }

    private fun sendToSignIn() {
        val intent = Intent(this, SignInActivity::class.java).apply {
            //clear navigation stack
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        startActivity(intent)
        finish()
    }

    private fun getCardWidth(): Int {
        val metrics = this.resources.displayMetrics
        val densityPerPixel = metrics.widthPixels / metrics.density
        return densityPerPixel.toInt()
    }

    companion object {
        private const val TAG = "ProfileActivity"
    }
}