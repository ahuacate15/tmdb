package com.antares.tmdb.ui.login

import androidx.lifecycle.viewModelScope
import com.antares.domain.interactor.auth.CreateTemporaryTokenUseCase
import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.auth.TmpToken
import com.antares.tmdb.utils.GenericViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val createTemporaryTokenUseCase: CreateTemporaryTokenUseCase,
    private val ioDispatcher: CoroutineDispatcher
) : GenericViewModel<TmpToken>() {

    fun createToken() {
        _dataObserver.value = State.Loading()

        viewModelScope.launch(ioDispatcher) {
            when (val result = createTemporaryTokenUseCase.invoke()) {
                is ResultWrapper.Success -> {
                    _dataObserver.postValue(
                        State.Success(result.data)
                    )
                }
                is ResultWrapper.GenericError -> {
                    _dataObserver.postValue(
                        State.Error(result.message)
                    )
                }
                is ResultWrapper.NetworkError -> {
                    _dataObserver.postValue(
                        State.Error()
                    )
                }
            }
        }
    }
}