package com.antares.tmdb.ui.season

import androidx.lifecycle.viewModelScope
import com.antares.domain.interactor.season.FindSeasonByIdShowAndSeasonNumberUseCase
import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.Season
import com.antares.tmdb.utils.GenericViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SeasonViewModel @Inject constructor(
    private val findSeasonByIdShowAndSeasonNumberUseCase: FindSeasonByIdShowAndSeasonNumberUseCase,
    private val ioDispatcher: CoroutineDispatcher
) : GenericViewModel<Season>() {

    fun findSeasonByIdShowAndSeasonNumber(idTvShow: Int, seasonNumber: Int) {
        _dataObserver.value = State.Loading()
        viewModelScope.launch(ioDispatcher) {
            when (val result =
                findSeasonByIdShowAndSeasonNumberUseCase.invoke(idTvShow, seasonNumber)) {
                is ResultWrapper.Success -> {
                    _dataObserver.postValue(
                        State.Success(result.data)
                    )
                }
                is ResultWrapper.GenericError -> {
                    _dataObserver.postValue(
                        State.Error(result.message)
                    )
                }
                is ResultWrapper.NetworkError -> {
                    _dataObserver.postValue(
                        State.Error()
                    )
                }
            }
        }
    }
}