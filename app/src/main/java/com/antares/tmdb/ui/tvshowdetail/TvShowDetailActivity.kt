package com.antares.tmdb.ui.tvshowdetail

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.antares.domain.model.TvShow
import com.antares.tmdb.R
import com.antares.tmdb.databinding.ActivityTvShowDetailBinding
import com.antares.tmdb.model.TvShowParcelable
import com.antares.tmdb.ui.season.SeasonActivity
import com.antares.tmdb.ui.seasonlist.SeasonListAdapter
import com.antares.tmdb.utils.Constant
import com.antares.tmdb.utils.GenericViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TvShowDetailActivity : AppCompatActivity() {

    private val viewModel: TvShowDetailViewModel by viewModels()
    private lateinit var binding: ActivityTvShowDetailBinding
    private lateinit var seasonListAdapter: SeasonListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_tv_show_detail)

        //hide navigation bar and set back button
        supportActionBar?.hide()

        //api 26 needs set color programmatically
        binding.cardFavorite.setBackgroundColor(ContextCompat.getColor(this, R.color.transparent))
        binding.cardBackList.setBackgroundColor(ContextCompat.getColor(this, R.color.transparent))
        binding.imgBackList.setColorFilter(ContextCompat.getColor(this, R.color.white))
        binding.imgFavorite.setColorFilter(ContextCompat.getColor(this, R.color.white))

        binding.cardBackList.setOnClickListener { onBackPressed() }

        intent.extras?.let {
            val tvShow: TvShowParcelable? = it.getParcelable(Constant.PARAM_PARCELABLE_TV_SHOW)
            tvShow?.let { safeTvShowParcelable ->
                loadTvShowData(safeTvShowParcelable)

            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        onBackPressed()
        return true
    }

    private fun loadTvShowData(tvShowParcelable: TvShowParcelable) {
        val tvShow = tvShowParcelable.mapToDomain()
        binding.tvShow = tvShow

        setSeasonAdapter(tvShow.id)
        observeTvShow(tvShow.id)
        observeMarkAsFavorite()
    }

    private fun setSeasonAdapter(idShow: Int) {
        seasonListAdapter = SeasonListAdapter {
            //open seasonDetail when user tap on recycler item
            val intent = Intent(this, SeasonActivity::class.java).apply {
                putExtra(Constant.PARAM_ID_TV_SHOW, idShow)
                putExtra(Constant.PARAM_SEASON_NUMBER, it.seasonNumber)
            }
            startActivity(intent)
        }
        binding.recyclewSeason.apply {
            //layoutManager = LinearLayoutManager(this@TvShowDetailActivity)
            adapter = seasonListAdapter
        }
    }

    private fun observeTvShow(idTvShow: Int) {
        viewModel.findTvShowById(idTvShow)
        viewModel.dataObserver.observe(this, { action ->
            when (action) {
                is GenericViewModel.State.Loading -> {
                    Log.d(TAG, "loading data...")
                }
                is GenericViewModel.State.Success -> {
                    seasonListAdapter.setData(action.data.seasons)
                    setTvShowOnClickListener(action.data)
                    setMarkAsFavoriteIcon(action.data.favorite)
                }
                is GenericViewModel.State.Error -> {
                    Toast.makeText(
                        this,
                        action.message ?: getString(R.string.err_load_tv_show),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })
    }

    private fun observeMarkAsFavorite() {
        viewModel.dataObserverMarkAsFavorite.observe(this, { action ->
            when (action) {
                is GenericViewModel.State.Loading -> {
                    Log.d(TAG, "marking as favorite...")
                }
                is GenericViewModel.State.Success -> {
                    setTvShowOnClickListener(action.data)
                    setMarkAsFavoriteIcon(action.data.favorite)
                }
                is GenericViewModel.State.Error -> {
                    Toast.makeText(
                        this,
                        action.message ?: getString(R.string.err_mark_favorite),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })
    }

    private fun setTvShowOnClickListener(tvShow: TvShow) {
        binding.cardFavorite.setOnClickListener {
            viewModel.markAsFavorite(
                idTvShow = tvShow.id,
                favorite = tvShow.favorite.not()
            )
        }
    }

    /**
     * change heart icon, depending on the value of favorite
     */
    private fun setMarkAsFavoriteIcon(favorite: Boolean) {
        if (favorite) {
            binding.imgFavorite.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.ic_baseline_favorite_24
                )
            )
        } else {
            binding.imgFavorite.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.ic_baseline_favorite_border_24
                )
            )
        }
    }

    companion object {
        private const val TAG = "TvShowDetailActivity"
    }
}