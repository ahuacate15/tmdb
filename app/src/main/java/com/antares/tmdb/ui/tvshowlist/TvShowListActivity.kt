package com.antares.tmdb.ui.tvshowlist

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.antares.tmdb.R
import com.antares.tmdb.databinding.ActivityTvShowListBinding
import com.antares.tmdb.extension.toParcelable
import com.antares.tmdb.ui.account.AccountActivity
import com.antares.tmdb.ui.tvshowdetail.TvShowDetailActivity
import com.antares.tmdb.utils.Constant
import com.antares.tmdb.utils.GenericViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TvShowListActivity : AppCompatActivity() {

    private val viewModel: TvShowListViewModel by viewModels()
    private lateinit var binding: ActivityTvShowListBinding
    private lateinit var showAdapter: TvShowListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_tv_show_list)

        //hide navigation bar
        supportActionBar?.hide()

        //api 26 needs set color programmatically
        binding.cardImgOpenAccount.setBackgroundColor(ContextCompat.getColor(this, R.color.purple_500))

        setAdapterListTvShow()
        displayTvShowList()
        setUIListeners()
    }

    private fun setAdapterListTvShow() {
        showAdapter = TvShowListAdapter(
            onClickListener = {
                val intent = Intent(this, TvShowDetailActivity::class.java).apply {
                    putExtra(Constant.PARAM_PARCELABLE_TV_SHOW, it.toParcelable())
                }
                startActivity(intent)
            }
        ).apply { setData(listOf(), false) }
        
        binding.recyclerShow.apply {
            layoutManager = StaggeredGridLayoutManager(calculateNumberOfColumns(), LinearLayoutManager.VERTICAL)
            adapter = showAdapter
        }

        binding.recyclerShow.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    Log.d(TAG, "end")
                    viewModel.loadNextPage()
                }
            }
        })

    }

    private fun displayTvShowList() {
        viewModel.findTopShow()
        viewModel.dataObserver.observe(this, { action ->
            when (action) {
                is GenericViewModel.State.Loading -> {
                    binding.progressLoadShows.visibility = View.VISIBLE
                }
                is GenericViewModel.State.Success -> {
                    binding.progressLoadShows.visibility = View.GONE
                    showAdapter.setData(
                        data = action.data.data.results,
                        appendData = action.data.appendData
                    )
                }
                is GenericViewModel.State.Error -> {
                    binding.progressLoadShows.visibility = View.GONE
                    Toast.makeText(
                        this,
                        action.message ?: getString(R.string.err_load_tv_show),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })
    }

    /**
     * calculate number of columns on recyclerView
     * return 2 by default when dpi is slow, avoiding show the list
     * on a single column, following the mock on figma
     */
    private fun calculateNumberOfColumns(): Int {
        val metrics = this.resources.displayMetrics
        val densityPerPixel = metrics.widthPixels / metrics.density
        val numberOfColumns = densityPerPixel.toInt() / Constant.UI_WIDTH_COLUMN
        return if (numberOfColumns >= Constant.UI_MIN_NUMBER_COLUMNS) numberOfColumns else Constant.UI_MIN_NUMBER_COLUMNS
    }

    private fun setUIListeners() {
        binding.cardImgOpenAccount.setOnClickListener {
            val intent = Intent(this, AccountActivity::class.java)
            startActivity(intent)
        }

        binding.chipGroup.setOnCheckedChangeListener { _, chipId ->
            filterShows(chipId)
        }
    }

    private fun filterShows(chipId: Int) {
        when (chipId) {
            binding.chipTopRated.id -> {
                viewModel.findTopShow()
            }
            binding.chipPopular.id -> {
                viewModel.findPopularShow()
            }
            binding.chipOnTv.id -> {
                viewModel.findOnTheAirShow()
            }
            binding.chipOnAirToday.id -> {
                viewModel.findOnAiringTodayShow()
            }
        }
    }

    companion object {
        private const val TAG = "HomeActivity"
    }
}