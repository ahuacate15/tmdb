package com.antares.tmdb.ui.tvshowlist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.antares.domain.model.TvShow
import com.antares.tmdb.R
import com.antares.tmdb.databinding.ItemShowBinding

class TvShowListAdapter(
    val isHorizontalList: Boolean = false,
    val cardWidth: Int = 450,
    val onClickListener: (TvShow) -> Unit
) :
    RecyclerView.Adapter<TvShowListAdapter.ShowAdapterViewHolder>() {

    private var list: MutableList<TvShow> = mutableListOf()

    class ShowAdapterViewHolder(val item: ItemShowBinding) : RecyclerView.ViewHolder(item.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowAdapterViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding =
            DataBindingUtil.inflate<ItemShowBinding>(inflater, R.layout.item_show, parent, false)
        return ShowAdapterViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ShowAdapterViewHolder, position: Int) {
        val tvShow = list[position]
        val isLastItem = position == list.size - 1

        holder.item.tvShow = tvShow
        holder.item.cardImgTv.setOnClickListener { onClickListener(tvShow) }

        //set width of cardView when list is horizontal
        if(isHorizontalList) {
            holder.item.cardShow.apply {
                val params = layoutParams as ViewGroup.MarginLayoutParams

                /**
                 * add extra margin on last horizontal item
                 * this amount prevents the card from sticking to
                 * the right side of the screen
                 */
                if(isLastItem) {
                    params.rightMargin = 50
                }

                params.width = cardWidth
                layoutParams = params
            }
        }
    }

    override fun getItemCount() = list.size

    fun setData(data: List<TvShow>, appendData: Boolean) {
        if (appendData) {
            val size = this.list.size
            val appendSize = data.size
            this.list.addAll(data)
            //update only new items
            notifyItemRangeChanged(size, size + appendSize)
        } else {
            this.list = data.toMutableList()
            notifyDataSetChanged()
        }
    }
}