package com.antares.tmdb.ui.season

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.antares.tmdb.R
import com.antares.tmdb.databinding.ActivitySeasonBinding
import com.antares.tmdb.ui.episodelist.EpisodeListAdapter
import com.antares.tmdb.utils.Constant
import com.antares.tmdb.utils.GenericViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SeasonActivity : AppCompatActivity() {

    private val viewModel: SeasonViewModel by viewModels()
    private lateinit var binding: ActivitySeasonBinding
    private val episodeListAdapter: EpisodeListAdapter = EpisodeListAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_season)

        //add back button
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //load parameters sent from previous activity
        intent.extras?.let {
            val idShow = it.getInt(Constant.PARAM_ID_TV_SHOW)
            val seasonNumber = it.getInt(Constant.PARAM_SEASON_NUMBER)

            //set navigation bar tittle
            supportActionBar?.title = getString(R.string.text_season) + " $seasonNumber"

            loadEpisodeAdapter(idShow, seasonNumber)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        onBackPressed()
        return true
    }

    private fun loadEpisodeAdapter(idShow: Int, seasonNumber: Int) {
        binding.recyclerEpisode.apply {
            layoutManager = LinearLayoutManager(this@SeasonActivity)
            adapter = episodeListAdapter
        }

        viewModel.findSeasonByIdShowAndSeasonNumber(idShow, seasonNumber)
        viewModel.dataObserver.observe(this, { action ->
            when (action) {
                is GenericViewModel.State.Loading -> {
                    Log.d(TAG, "loading...")
                    binding.progressLoadEpisodes.visibility = View.VISIBLE
                }
                is GenericViewModel.State.Success -> {
                    Log.d(TAG, "success: ${action.data}")
                    binding.progressLoadEpisodes.visibility = View.GONE
                    episodeListAdapter.setData(action.data.episodes)
                }
                is GenericViewModel.State.Error -> {
                    binding.progressLoadEpisodes.visibility = View.GONE
                    Toast.makeText(
                        this,
                        action.message ?: getString(R.string.err_load_season),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })
    }

    companion object {
        private const val TAG = "SeasonActivity"
    }
}