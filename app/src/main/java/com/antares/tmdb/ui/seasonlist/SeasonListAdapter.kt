package com.antares.tmdb.ui.seasonlist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.antares.domain.model.Season
import com.antares.tmdb.R
import com.antares.tmdb.databinding.ItemSeasonBinding

class SeasonListAdapter(val onClickListener: (Season) -> Unit) :
    RecyclerView.Adapter<SeasonListAdapter.SeasonAdapterViewHolder>() {

    private var list: MutableList<Season>? = mutableListOf()

    class SeasonAdapterViewHolder(val item: ItemSeasonBinding) : RecyclerView.ViewHolder(item.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeasonAdapterViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemSeasonBinding>(inflater, R.layout.item_season, parent, false)
        return SeasonAdapterViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SeasonAdapterViewHolder, position: Int) {
        list?.getOrNull(position)?.let { safeSeason ->
            holder.item.season = safeSeason
            holder.item.cardSeason.setOnClickListener { onClickListener(safeSeason) }
        }
    }

    override fun getItemCount(): Int = list?.size ?: 0

    fun setData(data: List<Season>?) {
        this.list = data?.toMutableList()
        notifyDataSetChanged()
    }
}