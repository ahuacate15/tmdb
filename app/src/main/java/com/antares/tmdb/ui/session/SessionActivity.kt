package com.antares.tmdb.ui.session

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.antares.tmdb.R
import com.antares.tmdb.databinding.ActivitySessionBinding
import com.antares.tmdb.ui.tvshowlist.TvShowListActivity
import com.antares.tmdb.utils.Constant
import com.antares.tmdb.utils.GenericViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SessionActivity : AppCompatActivity() {

    private val viewModel: SessionViewModel by viewModels()
    private lateinit var binding: ActivitySessionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_session)

        val parameters = intent.extras
        parameters?.let {
            val requestToken = it.get(Constant.PARAM_REQUEST_TOKEN) as String
            viewModel.createNewSession(requestToken)
        }

        viewModel.dataObserver.observe(this, { action ->
            when (action) {
                is GenericViewModel.State.Loading -> {
                    binding.pbCreatingSession.visibility = View.GONE
                }
                is GenericViewModel.State.Success -> {
                    //redirect to home
                    binding.pbCreatingSession.visibility = View.GONE
                    goToHome()
                }
                is GenericViewModel.State.Error -> {
                    binding.pbCreatingSession.visibility = View.GONE
                    binding.tCreatingSession.text =
                        action.message ?: getString(R.string.err_create_session)
                }
            }
        })
    }

    private fun goToHome() {
        val intent = Intent(this, TvShowListActivity::class.java)

        //clear navigation stack, avoid go back
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }
}