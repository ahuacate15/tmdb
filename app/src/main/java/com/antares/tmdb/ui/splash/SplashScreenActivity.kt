package com.antares.tmdb.ui.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import com.antares.tmdb.R
import com.antares.tmdb.databinding.ActivitySplashScreenBinding
import com.antares.tmdb.ui.signin.SignInActivity
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashScreenActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySplashScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash_screen)

        //hide navigation bar
        supportActionBar?.hide()
        sendToSignInActivity()
    }

    private fun sendToSignInActivity() {
        lifecycleScope.launch {
            delay(2500)
            val intent = Intent(this@SplashScreenActivity, SignInActivity::class.java).apply {
                //clear navigation stack
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            startActivity(intent)
            finish()
        }
    }
}