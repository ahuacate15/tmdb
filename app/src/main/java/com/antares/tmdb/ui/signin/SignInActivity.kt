package com.antares.tmdb.ui.signin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.antares.tmdb.R
import com.antares.tmdb.databinding.ActivitySignInBinding
import com.antares.tmdb.ui.tvshowlist.TvShowListActivity
import com.antares.tmdb.ui.login.LoginActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignInActivity : AppCompatActivity() {

    private val viewModel: SignInViewModel by viewModels()
    private lateinit var binding: ActivitySignInBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_in)

        //hide navigation bar
        supportActionBar?.hide()

        viewModel.verifySession()
        viewModel.dataSessionObserver.observe(this, { action ->
            when(action) {
                SignInViewModel.State.AlreadyLogin -> {
                    val intent = Intent(this, TvShowListActivity::class.java)
                    //clear navigation stack, avoid go back
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    finish()
                }
                SignInViewModel.State.NotLoggedYet -> {
                    binding.btnOpenWebViewLoging.setOnClickListener {
                        val intent = Intent(this, LoginActivity::class.java)
                        startActivity(intent)
                    }
                }
            }
        })
    }
}