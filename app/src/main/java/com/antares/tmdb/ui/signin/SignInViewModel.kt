package com.antares.tmdb.ui.signin

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.antares.tmdb.utils.Constant
import com.antares.tmdb.utils.SharedPreferencesManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignInViewModel @Inject constructor(
    private val sharedPreferencesManager: SharedPreferencesManager,
    private val ioDispatcher: CoroutineDispatcher
): ViewModel() {

    private val _dataSessionObserver: MutableLiveData<State> =
        MutableLiveData()
    val dataSessionObserver: LiveData<State> = _dataSessionObserver

    fun verifySession() {
        viewModelScope.launch(ioDispatcher) {
            val session = sharedPreferencesManager.getString(Constant.PREF_SESSION_ID)

            if (session.isNullOrEmpty()) {
                _dataSessionObserver.postValue(State.NotLoggedYet)
            } else {
                _dataSessionObserver.postValue(State.AlreadyLogin)
            }
        }
    }

    sealed class State {
        object AlreadyLogin : State()
        object NotLoggedYet : State()
    }
}