package com.antares.tmdb.ui.account

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.antares.domain.interactor.account.GetFavoriteTvShowUseCase
import com.antares.domain.interactor.auth.DeleteSessionUseCase
import com.antares.domain.interactor.auth.GetAccountUseCase
import com.antares.domain.model.ResultTvShow
import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.auth.Account
import com.antares.domain.model.auth.DeleteSession
import com.antares.tmdb.utils.Constant
import com.antares.tmdb.utils.GenericViewModel
import com.antares.tmdb.utils.SharedPreferencesManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AccountViewModel @Inject constructor(
    private val getAccountUseCase: GetAccountUseCase,
    private val deleteSessionUseCase: DeleteSessionUseCase,
    private val getFavoriteTvShowUseCase: GetFavoriteTvShowUseCase,
    private val sharedPreferencesManager: SharedPreferencesManager,
    private val ioDispatcher: CoroutineDispatcher
) : GenericViewModel<Account>() {

    //delete session observables
    private val _dataObserverDeleteSession = MutableLiveData<State<DeleteSession>>()
    val dataObserverDeleteSession: LiveData<State<DeleteSession>> = _dataObserverDeleteSession

    //favorite tvShow observable
    private val _observerFavoriteTvShow = MutableLiveData<State<ResultTvShow>>()
    val observerFavoriteTvShow: LiveData<State<ResultTvShow>> = _observerFavoriteTvShow

    fun loadProfile() {
        _dataObserver.value = State.Loading()

        val sessionId = sharedPreferencesManager.getString(Constant.PREF_SESSION_ID)

        if (sessionId == null) {
            //session doesn't exists, send generic error
            _dataObserver.postValue(
                State.Error(
                    errorCode = Constant.ERROR_SESSION_NOT_EXIT
                )
            )
        } else {
            viewModelScope.launch(ioDispatcher) {
                when (val result = getAccountUseCase.invoke(sessionId)) {
                    is ResultWrapper.Success -> {
                        _dataObserver.postValue(State.Success(result.data))
                    }
                    is ResultWrapper.GenericError -> {
                        _dataObserver.postValue(State.Error(result.message))
                    }
                    is ResultWrapper.NetworkError -> {
                        _dataObserver.postValue(State.Error())
                    }
                }
            }
        }
    }

    fun getFavoriteTvShow() {
        _observerFavoriteTvShow.value = State.Loading()

        //get sessionId
        val sessionId = sharedPreferencesManager.getString(Constant.PREF_SESSION_ID) ?: ""

        viewModelScope.launch(ioDispatcher) {
            when (val result = getFavoriteTvShowUseCase.invoke(sessionId)) {
                is ResultWrapper.Success -> {
                    _observerFavoriteTvShow.postValue(State.Success(result.data))
                }
                is ResultWrapper.GenericError -> {
                    _observerFavoriteTvShow.postValue(State.Error(result.message))
                }
                is ResultWrapper.NetworkError -> {
                    _observerFavoriteTvShow.postValue(State.Error())
                }
            }
        }

    }

    fun deleteSession() {
        _dataObserverDeleteSession.value = State.Loading()

        //load session id
        val sessionId = sharedPreferencesManager.getString(Constant.PREF_SESSION_ID)

        //session doesn't exits, the process continue with success state
        if (sessionId == null) {
            _dataObserverDeleteSession.postValue(
                State.Success(DeleteSession(true))
            )
        } else {
            viewModelScope.launch(ioDispatcher) {
                when (val result = deleteSessionUseCase.invoke(sessionId)) {
                    is ResultWrapper.Success -> {
                        sharedPreferencesManager.clearData()
                        _dataObserverDeleteSession.postValue(State.Success(result.data))
                    }
                    is ResultWrapper.GenericError -> {
                        _dataObserverDeleteSession.postValue(State.Error(result.message))
                    }
                    is ResultWrapper.NetworkError -> {
                        _dataObserverDeleteSession.postValue(State.Error())
                    }
                }
            }
        }
    }
}