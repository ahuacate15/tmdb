package com.antares.tmdb.ui.login

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.antares.tmdb.R
import com.antares.tmdb.databinding.ActivityLoginBinding
import com.antares.tmdb.ui.session.SessionActivity
import com.antares.tmdb.utils.Constant
import com.antares.tmdb.utils.CustomWebView
import com.antares.tmdb.utils.GenericViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {

    private val viewModel: LoginViewModel by viewModels()
    private lateinit var binding: ActivityLoginBinding

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //hide navigation bar
        supportActionBar?.hide()

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.webLogin.settings.javaScriptEnabled = true

        Log.d("LoginActivity", "onCreate...")
        viewModel.createToken()
        viewModel.dataObserver.observe(this, { action ->
            when (action) {
                is GenericViewModel.State.Loading -> {
                    binding.pbLoadingWebView.visibility = View.VISIBLE
                }
                is GenericViewModel.State.Success -> {
                    binding.webLogin.webViewClient =
                        CustomWebView(
                            onInterceptURL = { url ->
                                interceptLoginResponse(
                                    url,
                                    action.data.requestToken
                                )
                            },
                            onPageFinished = {
                                binding.pbLoadingWebView.visibility = View.GONE
                            }
                        )
                    binding.webLogin.loadUrl("https://www.themoviedb.org/authenticate/${action.data.requestToken}?redirect_to=${Constant.LOGIN_APP_ID}")
                }
                is GenericViewModel.State.Error -> {
                    Toast.makeText(
                        this,
                        action.message ?: getString(R.string.err_request_tmp_token),
                        Toast.LENGTH_SHORT
                    ).show()
                    onBackPressed()
                }
            }

        })
    }

    private fun interceptLoginResponse(url: String, requestToken: String): Boolean {
        return when {
            //login success, open createSessionActivity and send requestToken value
            url.startsWith(Constant.LOGIN_APP_ID) && url.endsWith("approved=true") -> {
                val intent = Intent(this, SessionActivity::class.java)
                intent.putExtra(Constant.PARAM_REQUEST_TOKEN, requestToken)

                //clear navigation stack, avoid go back to login activity
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

                startActivity(intent)
                finish()
                true
            }
            //login fail, send to mainActivity
            url.endsWith("denied=true") -> {
                Toast.makeText(
                    this,
                    "You do not have permission to access the app",
                    Toast.LENGTH_SHORT
                ).show()
                finish()
                true
            }
            //normal url handling from webView
            else -> {
                false
            }
        }
    }
}