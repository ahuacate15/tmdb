package com.antares.tmdb.ui.episodelist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.antares.domain.model.Episode
import com.antares.tmdb.R
import com.antares.tmdb.databinding.ItemEpisodeBinding

class EpisodeListAdapter : RecyclerView.Adapter<EpisodeListAdapter.EpisodeListAdapterViewHolder>() {

    private var list: MutableList<Episode>? = mutableListOf()

    class EpisodeListAdapterViewHolder(val item: ItemEpisodeBinding) :
        RecyclerView.ViewHolder(item.root)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): EpisodeListAdapterViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemEpisodeBinding>(
            inflater,
            R.layout.item_episode,
            parent,
            false
        )
        return EpisodeListAdapterViewHolder(binding)
    }

    override fun onBindViewHolder(holder: EpisodeListAdapterViewHolder, position: Int) {
        list?.getOrNull(position)?.let { safeEpisode ->
            holder.item.episode = safeEpisode
        }
    }

    override fun getItemCount() = list?.size ?: 0

    fun setData(data: List<Episode>?) {
        this.list = data?.toMutableList()
        notifyDataSetChanged()
    }

}