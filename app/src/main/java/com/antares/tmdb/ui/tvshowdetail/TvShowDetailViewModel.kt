package com.antares.tmdb.ui.tvshowdetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.antares.domain.interactor.account.MarkTvShowAsFavoriteUseCase
import com.antares.domain.interactor.tvshow.FindTvShowByIdUseCase
import com.antares.domain.model.ResultWrapper
import com.antares.domain.model.TvShow
import com.antares.tmdb.utils.Constant
import com.antares.tmdb.utils.GenericViewModel
import com.antares.tmdb.utils.SharedPreferencesManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TvShowDetailViewModel @Inject constructor(
    private val findTvShowByIdUseCase: FindTvShowByIdUseCase,
    private val markTvShowAsFavoriteUseCase: MarkTvShowAsFavoriteUseCase,
    private val sharedPreferencesManager: SharedPreferencesManager,
    private val ioDispatcher: CoroutineDispatcher
) : GenericViewModel<TvShow>() {

    //mark tvShow as favorite
    private val _dataObserverMarkAsFavorite =
        MutableLiveData<State<TvShow>>()
    val dataObserverMarkAsFavorite: LiveData<State<TvShow>> =
        _dataObserverMarkAsFavorite

    fun findTvShowById(id: Int) {
        _dataObserver.value = State.Loading()
        viewModelScope.launch(ioDispatcher) {
            when (val result = findTvShowByIdUseCase.invoke(id)) {
                is ResultWrapper.Success -> {
                    _dataObserver.postValue(
                        State.Success(result.data)
                    )
                }
                is ResultWrapper.NetworkError -> {
                    _dataObserver.postValue(
                        State.Error()
                    )
                }
                is ResultWrapper.GenericError -> {
                    _dataObserver.postValue(
                        State.Error(result.message)
                    )
                }
            }
        }
    }

    fun markAsFavorite(idTvShow: Int, favorite: Boolean) {
        _dataObserverMarkAsFavorite.value = State.Loading()
        val sessionId = sharedPreferencesManager.getString(Constant.PREF_SESSION_ID) ?: ""
        viewModelScope.launch(ioDispatcher) {
            when (val result = markTvShowAsFavoriteUseCase.invoke(sessionId, idTvShow, favorite)) {
                is ResultWrapper.Success -> {
                    _dataObserverMarkAsFavorite.postValue(
                        State.Success(result.data)
                    )
                }
                is ResultWrapper.GenericError -> {
                    _dataObserverMarkAsFavorite.postValue(
                        State.Error(result.message)
                    )
                }
                is ResultWrapper.NetworkError -> {
                    _dataObserverMarkAsFavorite.postValue(
                        State.Error()
                    )
                }
            }
        }
    }
}