package com.antares.tmdb.ui.tvshowlist

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.antares.domain.interactor.tvshow.FindByPopularTvShowUseCase
import com.antares.domain.interactor.tvshow.FindOnAiringTodayShowUseCase
import com.antares.domain.interactor.tvshow.FindOnTheAirShowUseCase
import com.antares.domain.interactor.tvshow.FindTopTvShowUseCase
import com.antares.domain.model.ResultTvShow
import com.antares.domain.model.ResultWrapper
import com.antares.tmdb.utils.GenericViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TvShowListViewModel @Inject constructor(
    private val findTopTvShowUseCase: FindTopTvShowUseCase,
    private val findByPopularTvShowUseCase: FindByPopularTvShowUseCase,
    private val findOnTheAirShowUseCase: FindOnTheAirShowUseCase,
    private val findOnAiringTodayShowUseCase: FindOnAiringTodayShowUseCase,
    private val ioDispatcher: CoroutineDispatcher
) : GenericViewModel<TvShowListViewModel.ResultState>() {

    fun findTopShow(page: Int = 1, appendData: Boolean = false) {
        _dataObserver.value = State.Loading()
        viewModelScope.launch(ioDispatcher) {
            updateDataObserver(findTopTvShowUseCase.invoke(page), appendData)
        }
    }

    fun findPopularShow(page: Int = 1, appendData: Boolean = false) {
        _dataObserver.value = State.Loading()
        viewModelScope.launch(ioDispatcher) {
            updateDataObserver(findByPopularTvShowUseCase.invoke(page), appendData)
        }
    }

    fun findOnTheAirShow(page: Int = 1, appendData: Boolean = false) {
        _dataObserver.value = State.Loading()
        viewModelScope.launch(ioDispatcher) {
            updateDataObserver(findOnTheAirShowUseCase.invoke(page), appendData)
        }
    }

    fun findOnAiringTodayShow(page: Int = 1, appendData: Boolean = false) {
        _dataObserver.value = State.Loading()
        viewModelScope.launch(ioDispatcher) {
            updateDataObserver(findOnAiringTodayShowUseCase.invoke(page), appendData)
        }
    }

    private fun updateDataObserver(result: ResultWrapper<ResultTvShow>, appendData: Boolean) {
        Log.d(TAG, "fetching list of shows: $result")
        when (result) {
            is ResultWrapper.Success -> {
                _dataObserver.postValue(
                    State.Success(
                        ResultState(
                            appendData = appendData,
                            data = result.data
                        )
                    )
                )
            }
            is ResultWrapper.GenericError -> {
                _dataObserver.postValue(
                    State.Error(result.message)
                )
            }
            is ResultWrapper.NetworkError -> {
                _dataObserver.postValue(
                    State.Error()
                )
            }
        }
    }

    fun loadNextPage() {
        dataObserver.value.let {
            Log.d(TAG, "loadNextPage: $it")
            when (it) {
                is State.Success -> {
                    if (it.data.data.page < it.data.data.totalPages) {
                        Log.d(TAG, "loading next page...: ${it.data.data.page + 1}")
                        findTopShow(
                            page = it.data.data.page + 1,
                            appendData = true
                        )
                    }
                }
            }
        }
    }

    class ResultState(
        val appendData: Boolean,
        val data: ResultTvShow
    )

    companion object {
        private const val TAG = "HomeViewModel"
    }
}
