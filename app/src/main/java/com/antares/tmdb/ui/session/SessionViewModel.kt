package com.antares.tmdb.ui.session

import androidx.lifecycle.viewModelScope
import com.antares.domain.interactor.account.SaveAccountDataUseCase
import com.antares.domain.interactor.auth.CreateNewSessionUseCase
import com.antares.domain.model.ResultWrapper
import com.antares.tmdb.utils.Constant
import com.antares.tmdb.utils.GenericViewModel
import com.antares.tmdb.utils.SharedPreferencesManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SessionViewModel @Inject constructor(
    private val createNewSessionUseCase: CreateNewSessionUseCase,
    private val saveAccountDataUseCase: SaveAccountDataUseCase,
    private val sharedPreferencesManager: SharedPreferencesManager,
    private val ioDispatcher: CoroutineDispatcher
) : GenericViewModel<Any>() {

    fun createNewSession(requestToken: String) {
        _dataObserver.value = State.Loading()
        viewModelScope.launch(ioDispatcher) {
            when (val result = createNewSessionUseCase.invoke(requestToken)) {
                is ResultWrapper.Success -> {

                    //save sessionId on shared preferences
                    sharedPreferencesManager.setString(
                        Constant.PREF_SESSION_ID,
                        result.data.sessionId
                    )

                    /**
                     * save account data on roomDB cache
                     * id_account is needed to check/uncheck shows as favorite
                     */
                    saveAccountDataUseCase.invoke(result.data.sessionId)

                    _dataObserver.postValue(
                        State.Success(true)
                    )
                }
                is ResultWrapper.GenericError -> {
                    _dataObserver.postValue(
                        State.Error(result.message)
                    )
                }

                is ResultWrapper.NetworkError -> {
                    _dataObserver.postValue(
                        State.Error()
                    )
                }
            }
        }
    }
}