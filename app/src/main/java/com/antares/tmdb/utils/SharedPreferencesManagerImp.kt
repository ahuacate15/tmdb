package com.antares.tmdb.utils

import android.content.Context

class SharedPreferencesManagerImp(context: Context) : SharedPreferencesManager {

    private val sharedPreferences = context.getSharedPreferences(SETTING_FILE, Context.MODE_PRIVATE)

    override fun getString(key: String) = sharedPreferences.getString(key, null)

    override fun setString(key: String, value: String) {
        sharedPreferences.edit().putString(key, value).apply()
    }

    override fun clearData() {
        sharedPreferences.edit().clear().apply()
    }

    companion object {
        private const val SETTING_FILE = "SETTING_FILE"
    }
}