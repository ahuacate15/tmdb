package com.antares.tmdb.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class GenericViewModel<T> : ViewModel() {

    protected val _dataObserver = MutableLiveData<State<T>>()
    val dataObserver: LiveData<State<T>> = _dataObserver

    sealed class State<T> {
        class Loading<T> : State<T>()
        class Success<T>(val data: T) : State<T>()
        class Error<T>(val message: String? = null, val errorCode: Int = Constant.ERROR_GENERIC) :
            State<T>()
    }
}