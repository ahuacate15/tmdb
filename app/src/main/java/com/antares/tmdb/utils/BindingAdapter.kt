package com.antares.tmdb.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.antares.tmdb.R
import com.bumptech.glide.Glide

object BindingAdapter {

    @JvmStatic
    @BindingAdapter("loadImage")
    fun loadImage(view: ImageView, url: String?) {
        if(url != null) {
            Glide.with(view.context)
                .load(Constant.API_IMAGE + url)
                .placeholder(R.drawable.default_title_poster)
                .into(view)
        } else {
            Glide.with(view.context)
                .load(R.drawable.default_title_poster)
                .into(view)
        }
    }
}