package com.antares.tmdb.utils

import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient

class CustomWebView(val onInterceptURL: (url: String) -> Boolean, val onPageFinished: () -> Unit) :
    WebViewClient() {

    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {

        //when conditions are true, the link doesn't open
        if (request?.url != null && onInterceptURL(request.url.toString())) {
            return true
        }
        //default url handling
        return super.shouldOverrideUrlLoading(view, request)
    }

    override fun onPageFinished(view: WebView?, url: String?) {
        super.onPageFinished(view, url)
        onPageFinished()
    }
}