package com.antares.tmdb.utils

interface SharedPreferencesManager {
    fun getString(key: String): String?
    fun setString(key: String, value: String)
    fun clearData()
}