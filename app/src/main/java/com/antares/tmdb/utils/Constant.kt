package com.antares.tmdb.utils

class Constant {

    companion object {

        //parameters passed between activities and fragments
        const val PARAM_REQUEST_TOKEN = "request_token"
        const val PARAM_ID_TV_SHOW = "id_show"
        const val PARAM_SEASON_NUMBER = "number_season"
        const val PARAM_PARCELABLE_TV_SHOW = "parcelable_tv_show"

        //shared preferences
        const val PREF_SESSION_ID = "session_id"

        //api parameters
        const val API_IMAGE = "https://image.tmdb.org/t/p/w500"

        //errors
        const val ERROR_GENERIC = 0
        const val ERROR_SESSION_NOT_EXIT = 1

        //UI
        const val UI_WIDTH_COLUMN = 180
        const val UI_MIN_NUMBER_COLUMNS = 2

        //other
        const val LOGIN_APP_ID = "app://com.antares.tmdb"
    }
}